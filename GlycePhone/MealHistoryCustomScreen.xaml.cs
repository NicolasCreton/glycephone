﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using WinRTXamlToolkit.Controls.DataVisualization.Charting;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace GlycePhone
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MealHistoryCustomScreen : GlycePhonePage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MealHistoryCustomScreen"/> class.
        /// This class represent the screen where we display an history of meal and a chart based on the date given by the patient
        /// </summary>
        public MealHistoryCustomScreen()
        {
            this.InitializeComponent();
            this.NeedSaving = false;
            this.IsRotation = true;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// We make a list a meal eaten based on the dates and we draw the chart by giving the query to the HistoryChart
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            List<DateTime> objDate = e.Parameter as List<DateTime>;  

            var query = (from InsulineCalculus in this.Model.MealHistoryList
                         where InsulineCalculus.DateHour > objDate[0] && InsulineCalculus.DateHour < objDate[1]
                         select InsulineCalculus).ToList();

            List<string> historyMealListString = new List<string>();

            foreach (InsulineCalculus meal in query)
            {
                historyMealListString.Add(meal.ToString());
            }
            this.HistoryBox.ItemsSource = historyMealListString;

            (this.HistoryChart.Series[0] as ColumnSeries).ItemsSource = query;
        }
    }
}
