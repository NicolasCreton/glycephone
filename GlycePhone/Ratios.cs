﻿using GlycePhone;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GlycePhonePatient
{
    [DataContract]
    public class Ratios
    {
        /// <summary>
        /// Gets a value indicating whether this <see cref="Ratios"/> is empty.
        /// </summary>
        /// <value>
        ///   <c>true</c> if empty; otherwise, <c>false</c>.
        /// </value>
        public bool Empty
        {
            get
            {
                return this.Values.Count == 0;
            }
        }

        /// <summary>
        /// Gets or sets the patient.
        /// </summary>
        /// <value>
        /// The patient.
        /// </value>
        [IgnoreDataMember]
        public Patient Patient { get; set; }

        /// <summary>
        /// Gets or sets the values.
        /// </summary>
        /// <value>
        /// The values.
        /// </value>
        [DataMember]
        public List<Ratio> Values { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Ratios"/> class.
        /// </summary>
        public Ratios()
            : this(null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Ratios"/> class.
        /// </summary>
        /// <param name="patient">The patient.</param>
        public Ratios(Patient patient)
        {
            this.Values = new List<Ratio>();
            this.Patient = patient;
        }

        /// <summary>
        /// Gets or sets the <see cref="System.Single"/> with the specified meal type.
        /// </summary>
        /// <value>
        /// The <see cref="System.Single"/>.
        /// </value>
        /// <param name="mealType">Type of the meal.</param>
        /// <returns></returns>
        public float this[MealType mealType]
        {
            get
            {
                Ratio ratio = Find(mealType);
                if (ratio != null)
                {
                    return ratio.Value;
                }
                return Ratio.NOT_SPECIFIED;
            }
            set
            {
                if (value == Ratio.NULL)
                {
                    Remove(mealType);
                }
                else
                {
                    Add(mealType, value);
                }
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="System.Single"/> with the specified meal name.
        /// </summary>
        /// <value>
        /// The <see cref="System.Single"/>.
        /// </value>
        /// <param name="mealName">Name of the meal.</param>
        /// <returns></returns>
        public float this[string mealName]
        {
            get
            {
                Ratio ratio = Find(mealName);
                if (ratio != null)
                {
                    return ratio.Value;
                }
                return Ratio.NOT_SPECIFIED;
            }
            set
            {
                if (value == Ratio.NULL)
                {
                    Remove(mealName);
                }
                else
                {
                    Add(mealName, value);
                }
            }
        }

        /// <summary>
        /// Finds the specified meal type.
        /// </summary>
        /// <param name="mealType">Type of the meal.</param>
        /// <returns></returns>
        public Ratio Find(MealType mealType)
        {
            if (mealType != null)
            {
                return this.Values.Find(ratio => mealType.Equals(ratio.MealType));
            }
            return null;
        }

        /// <summary>
        /// Finds the specified meal name.
        /// </summary>
        /// <param name="mealName">Name of the meal.</param>
        /// <returns></returns>
        public Ratio Find(string mealName)
        {
            return this.Values.Find(ratio => (mealName == ratio.MealType.Name));
        }

        /// <summary>
        /// Removes the specified meal type.
        /// </summary>
        /// <param name="mealType">Type of the meal.</param>
        public void Remove(MealType mealType)
        {
            Remove(Find(mealType));
        }

        /// <summary>
        /// Removes the specified meal name.
        /// </summary>
        /// <param name="mealName">Name of the meal.</param>
        public void Remove(string mealName)
        {
            Remove(Find(mealName));
        }

        /// <summary>
        /// Removes the specified ratio.
        /// </summary>
        /// <param name="ratio">The ratio.</param>
        public void Remove(Ratio ratio)
        {
            if (ratio != null)
            {
                this.Values.Remove(ratio);
            }
        }

        /// <summary>
        /// Adds the specified meal name.
        /// </summary>
        /// <param name="mealName">Name of the meal.</param>
        /// <param name="coef">The coef.</param>
        public void Add(string mealName, float coef)
        {
            Ratio ratio = Find(mealName);
            if (ratio == null)
            {
                this.Values.Add(new Ratio(new MealType(mealName), coef));
            }
            else
            {
                ratio.Value = coef;
            }
        }

        /// <summary>
        /// Adds the specified meal type.
        /// </summary>
        /// <param name="mealType">Type of the meal.</param>
        /// <param name="coef">The coef.</param>
        public void Add(MealType mealType, float coef)
        {
            Ratio ratio = Find(mealType);
            if (ratio == null)
            {
                this.Values.Add(new Ratio(mealType, coef));
            }
            else
            {
                if (coef != Ratio.NOT_SPECIFIED && coef != Ratio.NULL)
                {
                    ratio.Value = coef;
                }
            }
        }

    }
}
