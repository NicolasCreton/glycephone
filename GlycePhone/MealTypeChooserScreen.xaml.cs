﻿using GlycePhoneModel;
using GlycePhonePatient;
using GlycePhoneViewInformation;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour en savoir plus sur le modèle d’élément Page vierge, consultez la page http://go.microsoft.com/fwlink/?LinkID=390556

namespace GlycePhone
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class MealTypeChooserScreen : GlycePhonePage
    {
        public List<string> MealTypeList { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MealTypeChooserScreen"/> class.
        /// This class represents a screen wher we display the meals in a day based on the ratios of the patient
        /// If the patient have'nt got any ratios, no meal is displayed
        /// </summary>
        public MealTypeChooserScreen()
        {
            this.InitializeComponent();
            this.IsRotation = true;
        }

        /// <summary>
        /// Invoqué lorsque cette page est sur le point d'être affichée dans un frame.
        /// </summary>
        /// <param name="e">Données d'événement décrivant la manière dont l'utilisateur a accédé à cette page.
        /// Ce paramètre est généralement utilisé pour configurer la page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            List<string> mealTypeStringList = new List<string>();
            foreach (Ratio ratio in this.Model.Patient.Ratios.Values)
            {
                if (ratio.Specified)
                {
                    mealTypeStringList.Add(ratio.MealType.ToString());
                }
            }
            this.TextTileLandingGrid.ItemsSource = mealTypeStringList;

            if (mealTypeStringList.Count != 0)
            {
                this.NoRatioBlock.Visibility = Visibility.Collapsed;
            }
            
        }

        private void Grid_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.Model.Calculus = new InsulineCalculus(this.Model);
            this.Model.Calculus.Meal = this.Model.DefaultMealTypes.Find(this.TextTileLandingGrid.SelectedItem.ToString());
            this.Navigate(typeof(FoodCategorieChooserScreen));
        }
    }
}
