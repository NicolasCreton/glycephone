﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GlycePhonePatient
{
    [DataContract]
    public class DefaultMealTypes
    {
        //Breakfast = 1, Lunch = 2, Dinner = 3, AfternoonSnack = 4

        /// <summary>
        /// Gets or sets the types.
        /// </summary>
        /// <value>
        /// The types.
        /// </value>
        [DataMember]
        public List<MealType> Types { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultMealTypes"/> class.
        /// </summary>
        public DefaultMealTypes()
        {
            this.Types = new List<MealType>() { new MealType("Breakfast", "Petit Déjeuner"), new MealType("Lunch", "Déjeuner"), new MealType("Dinner", "Diner"), new MealType("Afternoon Snack", "Gouter") };
        }

        /// <summary>
        /// Finds the specified type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public MealType Find(string type)
        {
            foreach (MealType mealType in this.Types)
            {
                if (type == mealType.Name || type == mealType.WrittenName)
                {
                    return mealType;
                }
            }
            return null;
        }

    }
}
