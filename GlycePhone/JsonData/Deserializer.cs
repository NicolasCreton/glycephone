using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Storage;

namespace GlycePhone
{
    class Deserializer
    {
        /// <summary>
        /// This async function reads the JSON (with all the food) and make a list of food for the app
        /// </summary>
        /// <returns>A list a foodElement</returns>
        public static async Task<LinkedList<FoodElement>> deserialize()
        {
            Uri dataUri = new Uri("ms-appx:///JsonData/food.json");
            StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(dataUri);
            string jsonText = await FileIO.ReadTextAsync(file);
            JsonObject jsonObject = JsonObject.Parse(jsonText);
            JsonArray jsonArray = jsonObject["foodArray"].GetArray();
            LinkedList<FoodElement> foodElementList = new LinkedList<FoodElement>();

            foreach (JsonValue jvalue in jsonArray)
            {
                JsonObject jobject = jvalue.GetObject();
                FoodElement foodFromJson = new FoodElement(
                    jobject["Type"].GetString(),
                    jobject["Nom"].GetString(),
                    (float)jobject["Glucides"].GetNumber()
                    );
                foodElementList.AddLast(foodFromJson);
            }

            return foodElementList;
        }
    }
}
