﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GlycePhonePatient
{
    [DataContract]
    public class MealType
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the written name (french).
        /// </summary>
        /// <value>
        /// The name of the written.
        /// </value>
        [DataMember]
        public string WrittenName { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MealType"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public MealType(string name)
            : this(name, name)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MealType"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="writtenName">Name of the written.</param>
        public MealType(string name, string writtenName)
        {
            this.Name = name;
            this.WrittenName = writtenName;
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (obj is MealType)
            {
                MealType mealType = (MealType)obj;
                return (mealType.Name == Name && mealType.WrittenName == WrittenName);
            }
            return false;
        }

        public override string ToString()
        {
            return WrittenName;
        }
    }
}
