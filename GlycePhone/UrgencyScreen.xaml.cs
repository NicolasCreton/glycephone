﻿using GlycePhoneModel;
using System;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;
using Windows.Media.SpeechSynthesis;
using GlycePhonePatient;
using Windows.Graphics.Display;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace GlycePhone
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class UrgencyScreen : GlycePhonePage
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="UrgencyScreen"/> class.
        /// </summary>
        public UrgencyScreen()
        {
            this.InitializeComponent();
            this.IsRotation = true;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// This class represents the emergency screen where the patient can send a SMS or make a phone call to a contact to tell them there is somthing wrong
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.NoContactBlock.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// Handles the Tapped event of the HypoButton control.
        /// Alert a contact with SMS or phone call
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TappedRoutedEventArgs"/> instance containing the event data.</param>
        private void HypoButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (this.Model.Patient.Contacts.People.Count != 0)
            {
                if (this.Model.Patient.Contacts.EmergencyAction == EmergencyAction.Message)
                {
                    sendMessageUrgency("Hypoglycémie");
                }
                else if (this.Model.Patient.Contacts.EmergencyAction == EmergencyAction.Phone)
                {
                    makePhoneCallUrgency();
                }
            }
            else
            {
                this.NoContactBlock.Visibility = Visibility.Visible;
            }

        }

        /// <summary>
        /// Handles the Tapped event of the HyperButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TappedRoutedEventArgs"/> instance containing the event data.</param>
        private void HyperButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (this.Model.Patient.Contacts.People.Count != 0)
            {
                if (this.Model.Patient.Contacts.EmergencyAction == EmergencyAction.Message)
                {
                    sendMessageUrgency("Hyperglycémie");
                }
                else if (this.Model.Patient.Contacts.EmergencyAction == EmergencyAction.Phone)
                {
                    makePhoneCallUrgency();
                }
            }
            else
            {
                this.NoContactBlock.Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// Sends the message urgency.
        /// The SMS contains the type of emergency and the GPS position of the patient
        /// </summary>
        /// <param name="type">The type.</param>
        private async void sendMessageUrgency(string type)
        {
            Windows.ApplicationModel.Chat.ChatMessage msg = new Windows.ApplicationModel.Chat.ChatMessage();
            msg.Body = "URGENCE : " + this.Model.Patient.FirstName + " est en " + type + ".";
            foreach (Contact cont in this.Model.Patient.Contacts)
            {
                msg.Recipients.Add(cont.PhoneNumber);
            }

            Geoposition position = await getLocation();

            if (position != null)
            {
                msg.Body += " localisation GPS : " + position.Coordinate.Latitude.ToString("00.00") + ", " + position.Coordinate.Longitude.ToString("00.00");
            }


            await Windows.ApplicationModel.Chat.ChatMessageManager.ShowComposeSmsMessageAsync(msg);
        }

        /// <summary>
        /// Gets the location of the patient using the GPS in the phone
        /// </summary>
        /// <returns></returns>
        private async Task<Geoposition> getLocation()
        {
            Geolocator geolocator = new Geolocator();
            geolocator.DesiredAccuracyInMeters = 50;

            try
            {
                Geoposition geoposition = await geolocator.GetGeopositionAsync(
                     maximumAge: TimeSpan.FromMinutes(5),
                     timeout: TimeSpan.FromSeconds(10)
                    );
                return geoposition;
            }
            //If an error is catch 2 are the main causes: the first is that you forgot to include ID_CAP_LOCATION in your app manifest. 
            //The second is that the user doesn't turned on the Location Services
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Makes the phone call urgency to the first contact of his list
        /// </summary>
        private void makePhoneCallUrgency()
        {
            Contact contact = this.Model.Patient.Contacts[0];
            Windows.ApplicationModel.Calls.PhoneCallManager.ShowPhoneCallUI(contact.PhoneNumber, contact.FirstName);
        }

        /// <summary>
        /// Handles the Tapped event of the ReturnButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TappedRoutedEventArgs"/> instance containing the event data.</param>
        private void ReturnButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.Frame.GoBack();
        }



    }
}
