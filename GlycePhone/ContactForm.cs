﻿using GlycePhonePatient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Windows.UI.Xaml.Controls;

namespace GlycePhone
{
    /// <summary>
    /// Used by the InscriptionPageContacts
    /// </summary>
    public class ContactForm
    {
        /// <summary>
        /// Gets or sets the contact.
        /// </summary>
        /// <value>
        /// The contact.
        /// </value>
        public Contact Contact { get; set; }
        /// <summary>
        /// Gets or sets the name textbox
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public TextBox Name { get; set; }
        /// <summary>
        /// Gets or sets the phone textbox
        /// </summary>
        /// <value>
        /// The phone.
        /// </value>
        public TextBox Phone { get; set; }
        /// <summary>
        /// Gets or sets the delete button.
        /// </summary>
        /// <value>
        /// The delete button.
        /// </value>
        public AppBarButton DeleteButton { get; set; }

        /// <summary>
        /// Gets or sets the container.
        /// </summary>
        /// <value>
        /// The container.
        /// </value>
        public StackPanel Container { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContactForm"/> class.
        /// </summary>
        /// <param name="contact">The contact.</param>
        /// <param name="name">The name.</param>
        /// <param name="phone">The phone.</param>
        /// <param name="delete">The delete.</param>
        /// <param name="container">The container.</param>
        public ContactForm(Contact contact, TextBox name, TextBox phone, AppBarButton delete, StackPanel container)
        {
            this.Contact = contact;
            this.Name = name;
            this.Phone = phone;
            this.DeleteButton = delete;
            this.Container = container;
        }
    }
}
