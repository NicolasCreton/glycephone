﻿using GlycePhoneModel;
using GlycePhonePatient;
using GlycePhoneViewInformation;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace GlycePhone
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class InscriptionsPageRatios : GlycePhonePage
    {

        private LinkedList<RatioSlider> ratioSliders = new LinkedList<RatioSlider>();
        public InscriptionsPageRatios()
        {
            this.InitializeComponent();
            this.NeedSaving = true;
            this.IsRotation = true;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            foreach (Ratio ratio in Model.Patient.Ratios.Values)
            {
                StackPanel ratioGroup = new StackPanel()
                {
                    Orientation = Orientation.Vertical,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    Width = 400,
                };
                TextBlock sliderName = new TextBlock()
                {
                    Text = ratio.MealType.WrittenName,
                    FontSize = 20,
                };
                ratioGroup.Children.Add(sliderName);

                StackPanel ratioValueGroup = new StackPanel()
                {
                    Orientation = Orientation.Horizontal,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    Width = 400,
                };

                SolidColorBrush brush = new SolidColorBrush(Colors.Yellow);
                Slider ratioSlider = new Slider()
                {
                    Foreground = brush,
                    Name = ratio.MealType.Name,
                    StepFrequency = 0.1,
                    Maximum = 3,
                    Minimum = 0,
                    Width = 250,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    Padding = new Windows.UI.Xaml.Thickness(0, 0, 10, 0),
                };
                TextBox sliderValue = new TextBox()
                {
                    FontSize = 20,
                    Width = 40,
                    Height = 30,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,

                };
                this.ratioSliders.AddLast(new RatioSlider(ratioSlider, sliderValue));

                ratioSlider.ValueChanged += ratioSlider_ValueChanged;

                ratioValueGroup.Children.Add(ratioSlider);

                InputScopeName inputscopeName = new InputScopeName();
                InputScope inputscope = new InputScope();
                inputscopeName.NameValue = InputScopeNameValue.Number;
                inputscope.Names.Add(inputscopeName);
                sliderValue.InputScope = inputscope;
                sliderValue.TextChanged += sliderValue_TextChanged;

                ratioValueGroup.Children.Add(sliderValue);
                ratioGroup.Children.Add(ratioValueGroup);
                Test.Children.Add(ratioGroup);
                if (ratio.Value != Ratio.NULL && ratio.Value != Ratio.NOT_SPECIFIED)
                {
                    ratioSlider.Value = Math.Truncate(ratio.Value * 100) / 100;
                }
            }
        }

        /// <summary>
        /// Handles the ValueChanged event of the ratioSlider control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RangeBaseValueChangedEventArgs"/> instance containing the event data.</param>
        void ratioSlider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            RatioSlider ratioSlider = Find((Slider)sender);
            if (ratioSlider != null)
            {
                ratioSlider.Textbox.Text = ratioSlider.Slider.Value.ToString();
            }
        }



        /// <summary>
        /// Handles the TextChanged event of the sliderValue control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TextChangedEventArgs"/> instance containing the event data.</param>
        void sliderValue_TextChanged(object sender, TextChangedEventArgs e)
        {
            RatioSlider ratioSlider = Find((TextBox)sender);
            if (ratioSlider != null)
            {
                try
                {
                    ratioSlider.Slider.Value = Convert.ToDouble(ratioSlider.Textbox.Text);
                }
                catch (System.FormatException)
                {

                }
            }

        }

        /// <summary>
        /// Finds the specified slider.
        /// </summary>
        /// <param name="slider">The slider.</param>
        /// <returns></returns>
        private RatioSlider Find(Slider slider)
        {
            foreach (RatioSlider ratioSlider in this.ratioSliders)
            {
                if (ratioSlider.Slider == slider)
                {
                    return ratioSlider;
                }
            }
            return null;
        }

        /// <summary>
        /// Finds the specified text box.
        /// </summary>
        /// <param name="textBox">The text box.</param>
        /// <returns></returns>
        private RatioSlider Find(TextBox textBox)
        {
            foreach (RatioSlider ratioSlider in this.ratioSliders)
            {
                if (ratioSlider.Textbox == textBox)
                {
                    return ratioSlider;
                }
            }
            return null;
        }

        /// <summary>
        /// Handles the Click event of the ValidateButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void ValidateButton_Click(object sender, RoutedEventArgs e)
        {
            ValidateRatios();
            if (this.Model.Registered)
            {
                this.Navigate(typeof(ProfilPage));
            }
            else
            {
                this.Navigate(typeof(InscriptionPageContacts));
            }
        }

        /// <summary>
        /// Validates the ratios.
        /// </summary>
        private void ValidateRatios()
        {
            foreach (RatioSlider ratioSlider in this.ratioSliders)
            {
                this.Model.Patient.Ratios[ratioSlider.Slider.Name] = (float)ratioSlider.Slider.Value;
            }
        }

        private void GoBackButton_Click(object sender, RoutedEventArgs e)
        {
            this.GoBack();
        }
    }
}
