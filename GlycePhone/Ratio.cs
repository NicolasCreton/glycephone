﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GlycePhonePatient
{
    [DataContract]
    public class Ratio
    {
        public const int NOT_SPECIFIED = -1;
        public const int NULL = -2;

        /// <summary>
        /// Gets or sets the type of the meal.
        /// </summary>
        /// <value>
        /// The type of the meal.
        /// </value>
        [DataMember]
        public MealType MealType { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        [DataMember]
        public float Value { get; set; }

        /// <summary>
        /// Gets a value indicating whether this <see cref="Ratio"/> is specified.
        /// </summary>
        /// <value>
        ///   <c>true</c> if specified; otherwise, <c>false</c>.
        /// </value>
        public bool Specified
        {
            get
            {
                return (Value != NOT_SPECIFIED && Value != NULL);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Ratio"/> class.
        /// </summary>
        public Ratio()
            : this(null, NOT_SPECIFIED)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Ratio"/> class.
        /// </summary>
        /// <param name="mealType">Type of the meal.</param>
        /// <param name="value">The value.</param>
        public Ratio(MealType mealType, float value = NOT_SPECIFIED)
        {
            this.MealType = mealType;
            this.Value = NOT_SPECIFIED;
        }
    }
}
