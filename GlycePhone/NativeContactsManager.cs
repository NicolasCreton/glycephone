﻿using GlycePhonePatient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlycePhone
{
    public class NativeContactsManager
    {
        private static bool isLoading = false;
        private static Windows.ApplicationModel.Contacts.Contact loadedNativeContact = null;

        /// <summary>
        /// Picks the native contact synchronously and returns a GlycePhone contact
        /// </summary>
        /// <returns></returns>
        public static Contact PickContact()
        {
            Windows.ApplicationModel.Contacts.Contact nativeContact = PickNativeContact();
            Contact contact = ToContact(nativeContact);
            return contact;
        }

        /// <summary>
        /// Picks the native contact synchronously
        /// </summary>
        /// <returns></returns>
        public static Windows.ApplicationModel.Contacts.Contact PickNativeContact()
        {
            PickContactAsync();
            while (isLoading)
            {
                //Wait
            }
            return loadedNativeContact;
        }


        /// <summary>
        /// Converts a native Windows Contact to a GlycePhone Contact
        /// </summary>
        /// <param name="nativeContact">The native contact.</param>
        /// <returns></returns>
        public static Contact ToContact(Windows.ApplicationModel.Contacts.Contact nativeContact)
        {
            if (nativeContact == null)
            {
                return null;
            }
            string firstName = nativeContact.FirstName;
            string number = nativeContact.Phones.FirstOrDefault().Number;
            if (firstName == null || firstName == "" || number == null || number == "")
            {
                return null;
            }
            return new Contact(firstName, number);
        }

        /// <summary>
        /// Begins the loading.
        /// </summary>
        private static void BeginLoading()
        {
            isLoading = true;
            loadedNativeContact = null;
        }


        /// <summary>
        /// Ends the loading and gives the result value to the static field loadedNativeContact
        /// </summary>
        /// <param name="nativeContact">The native contact.</param>
        private static void EndLoading(Windows.ApplicationModel.Contacts.Contact nativeContact)
        {
            loadedNativeContact = nativeContact;
            isLoading = false;
        }

        /// <summary>
        /// Lets the user pick a contact in the contact app
        /// </summary>
        /// <returns></returns>
        public async static Task<Windows.ApplicationModel.Contacts.Contact> PickContactAsync()
        {
            BeginLoading();
            var contactPicker = new Windows.ApplicationModel.Contacts.ContactPicker();
            contactPicker.DesiredFieldsWithContactFieldType.Add(Windows.ApplicationModel.Contacts.ContactFieldType.PhoneNumber);
            var contact = await contactPicker.PickContactAsync();
            EndLoading(contact);
            return contact;
        }
    }
}
