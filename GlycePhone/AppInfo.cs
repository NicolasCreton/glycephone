﻿using GlycePhoneModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlycePhoneViewInformation
{
    public class AppInfo
    {
        /// <summary>
        /// DEBUG : Set to true if you want to have a new model (and not load the one from the storage)
        /// </summary>
        private const bool NEW_MODEL = false;

        private Model model;

        /// <summary>
        /// Gets or sets the model.
        /// </summary>
        /// <value>
        /// The model.
        /// </value>
        public Model Model
        {
            get { return model; }
            set { model = value; }
        }

        public AppInfo()
        {
            this.LoadModel();
        }

        /// <summary>
        /// Either loads the model from the storage or creates a new one
        /// </summary>
        private void LoadModel()
        {
            Model loadedModel = ModelManager.LoadModel();
            if (loadedModel != null)
            {
                if (!NEW_MODEL)
                {
                    this.Model = loadedModel;
                }
                else
                { //DEBUG : Make a new one and replace the existing one
                    this.Model = new Model();
                    this.Model.Save();
                }
            }
            else
            {
                this.Model = new Model();
            }
            this.Model.EndLoading();
        }
    }
}
