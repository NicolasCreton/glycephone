﻿using GlycePhoneModel;
using GlycePhoneViewInformation;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace GlycePhone
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class InscriptionPageName : GlycePhonePage
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="InscriptionPageName"/> class.
        /// </summary>
        public InscriptionPageName()
        {
            this.InitializeComponent();
            this.NeedSaving = true;
            this.IsRotation = true;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.FirstName.Text = this.Model.Patient.FirstName;
            this.LastName.Text = this.Model.Patient.LastName;
            if (Model.Patient.IsBoy)
            {
                this.RadioButton_Boy.IsChecked = true;
            }
            else
            {
                this.RadioButton_Girl.IsChecked = true;
            }
        }

        /// <summary>
        /// Handles the Click event of the ValidateButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void ValidateButton_Click(object sender, RoutedEventArgs e)
        {
            Validate();
        }

        /// <summary>
        /// Handles the GotFocus event of the Nom control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Nom_GotFocus(object sender, RoutedEventArgs e)
        {
            if (LastName.Text == "Nom")
            {
                LastName.Text = "";
            }
        }

        /// <summary>
        /// Handles the GotFocus event of the Prénom control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Prénom_GotFocus(object sender, RoutedEventArgs e)
        {
            if (FirstName.Text == "Prénom")
            {
                FirstName.Text = "";
            }
        }

        /// <summary>
        /// Handles the LostFocus event of the Nom control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Nom_LostFocus(object sender, RoutedEventArgs e)
        {
            if (LastName.Text == "")
            {
                LastName.Text = "Nom";
            }
        }

        /// <summary>
        /// Handles the LostFocus event of the Prénom control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Prénom_LostFocus(object sender, RoutedEventArgs e)
        {
            if (FirstName.Text == "")
            {
                FirstName.Text = "Prénom";
            }
        }

        /// <summary>
        /// Handles the KeyDown event of the LastName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyRoutedEventArgs"/> instance containing the event data.</param>
        private void LastName_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                this.LoseFocus(this.LastName);
            }
        }

        /// <summary>
        /// Handles the KeyDown event of the FirstName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyRoutedEventArgs"/> instance containing the event data.</param>
        private void FirstName_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                this.LastName.Focus(FocusState.Keyboard);
            }
            
        }

        /// <summary>
        /// Validates this instance.
        /// </summary>
        private void Validate()
        {
            Model.Patient.FirstName = FirstName.Text;
            Model.Patient.LastName = LastName.Text;
            this.Navigate(typeof(InscriptionPageAge));
        }

        /// <summary>
        /// Handles the Click event of the ExitButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Exit();
        }

        /// <summary>
        /// Handles the Checked event of the RadioButton_Boy control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void RadioButton_Boy_Checked(object sender, RoutedEventArgs e)
        {
            Model.Patient.IsBoy = true;
        }

        /// <summary>
        /// Handles the Checked event of the RadioButton_Girl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void RadioButton_Girl_Checked(object sender, RoutedEventArgs e)
        {
            Model.Patient.IsBoy = false;
        }
    }
}
