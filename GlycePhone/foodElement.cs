﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GlycePhone
{
    [DataContract]
    public class FoodElement
    {
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        [DataMember]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the glucide.
        /// </summary>
        /// <value>
        /// The glucide.
        /// </value>
        [DataMember]
        public float Glucide { get; set; }

        public FoodElement()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FoodElement"/> class.
        /// This class represents a food element with its name, type and glucide
        /// Certain members are Serializable
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="name">The name.</param>
        /// <param name="glucide">The glucide.</param>
        public FoodElement(string type, string name, float glucide)
        {
            this.Type = type;
            this.Name = name;
            this.Glucide = glucide;
        }
    }
}
