﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace GlycePhone
{
    public class RatioSlider
    {
        /// <summary>
        /// The slider
        /// </summary>
        private Slider slider;

        /// <summary>
        /// Gets or sets the slider.
        /// </summary>
        /// <value>
        /// The slider.
        /// </value>
        public Slider Slider
        {
            get { return slider; }
            set { slider = value; }
        }

        /// <summary>
        /// The textbox
        /// </summary>
        private TextBox textbox;

        /// <summary>
        /// Gets or sets the textbox linked to the slider
        /// </summary>
        /// <value>
        /// The textbox.
        /// </value>
        public TextBox Textbox
        {
            get { return textbox; }
            set { textbox = value; }
        }



        /// <summary>
        /// Initializes a new instance of the <see cref="RatioSlider"/> class.
        /// </summary>
        /// <param name="slider">The slider.</param>
        /// <param name="textbox">The textbox.</param>
        public RatioSlider(Slider slider, TextBox textbox)
        {
            this.Textbox = textbox;
            this.slider = slider;
        }
    }
}
