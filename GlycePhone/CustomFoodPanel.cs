﻿using GlycePhoneModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace GlycePhone
{
    /// <summary>
    /// Used by the Custom Food Panel in the Profil Page
    /// </summary>
    class CustomFoodPanel
    {
        /// <summary>
        /// Gets or sets the button.
        /// </summary>
        /// <value>
        /// The button.
        /// </value>
        public AppBarButton DeleteButton { get; set; }
        /// <summary>
        /// Gets or sets the food.
        /// </summary>
        /// <value>
        /// The food.
        /// </value>
        public TextBlock Food { get; set; }
        /// <summary>
        /// Gets or sets the glucides.
        /// </summary>
        /// <value>
        /// The glucides.
        /// </value>
        public TextBox Glucides { get; set; }
        /// <summary>
        /// Gets or sets the custom food element
        /// </summary>
        /// <value>
        /// The custom food.
        /// </value>
        public FoodElement CustomFood { get; set; }

        /// <summary>
        /// Gets or sets the container.
        /// </summary>
        /// <value>
        /// The container.
        /// </value>
        public StackPanel Container { get; set; }

        /// <summary>
        /// Gets or sets the profil page.
        /// </summary>
        /// <value>
        /// The profil page.
        /// </value>
        public ProfilPage ProfilPage { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomFoodPanel"/> class.
        /// </summary>
        /// <param name="foodElement">The food element.</param>
        /// <param name="profilPage">The profil page.</param>
        public CustomFoodPanel(FoodElement foodElement, ProfilPage profilPage)
        {
            this.ProfilPage = profilPage;
            this.DeleteButton = new AppBarButton();
            this.DeleteButton.Icon = new SymbolIcon(Symbol.Delete);
            this.DeleteButton.Click += RemoveButton_Click;
            this.DeleteButton.RenderTransform = new ScaleTransform() { ScaleX = 0.75, ScaleY = 0.75 };
            this.CustomFood = foodElement;

            StackPanel foodPanel = new StackPanel() { Orientation = Orientation.Vertical };
            foodPanel.Children.Add(new TextBlock() { Text = foodElement.Name, FontSize = 15, FontFamily = new FontFamily("/Fonts/Simpsonfont.ttf#Simpsonfont"), VerticalAlignment = VerticalAlignment.Center });
            foodPanel.Children.Add(new TextBlock() { Text = string.Format("{0}",foodElement.Glucide), FontSize = 10, FontFamily = new FontFamily("/Fonts/Simpsonfont.ttf#Simpsonfont"), VerticalAlignment = VerticalAlignment.Center });

            this.Container = new StackPanel();
            this.Container.Orientation = Orientation.Horizontal;
            this.Container.Children.Add(DeleteButton);
            this.Container.Children.Add(foodPanel);
        }

        void RemoveButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            this.ProfilPage.RemoveFoodElement(CustomFood);
            
        }
    }
}
