﻿using GlycePhone;
using GlycePhoneModel;
using GlycePhoneUtil;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GlycePhonePatient
{
    [DataContract]
    public class Patient : Person
    {
        /// <summary>
        /// Gets or sets the model.
        /// </summary>
        /// <value>
        /// The model.
        /// </value>
        [IgnoreDataMember]
        public Model Model { get; set; }

        /// <summary>
        /// Gets or sets the birthday.
        /// </summary>
        /// <value>
        /// The birthday.
        /// </value>
        [DataMember]
        public DateTime Birthday { get; set; }
        //Photo

        /// <summary>
        /// Gets or sets a value indicating whether this instance is a boy.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is boy; otherwise, <c>false</c>.
        /// </value>
        public bool IsBoy { get; set; }

        /// <summary>
        /// Gets the birth format.
        /// </summary>
        /// <value>
        /// The birth format.
        /// </value>
        [IgnoreDataMember]
        public string BirthFormat
        {
            get
            {
                return string.Format("{0:d/M/yyyy}", this.Birthday);
            }
        }

        private Ratios ratios;

        /// <summary>
        /// Gets or sets the ratios.
        /// </summary>
        /// <value>
        /// The ratios.
        /// </value>
        [DataMember]
        public Ratios Ratios
        {
            get { return ratios; }
            set
            {
                if (value == null)
                {
                    ratios = new Ratios(this);
                }
                else
                {
                    ratios = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the contacts.
        /// </summary>
        /// <value>
        /// The contacts.
        /// </value>
        [DataMember]
        public Contacts Contacts { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Patient"/> class.
        /// </summary>
        public Patient()
            : this(null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Patient"/> class.
        /// </summary>
        /// <param name="model">The model.</param>
        public Patient(Model model)
            : this(model, null, null, true, DateTime.Now, new Ratios(), new Contacts())
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Patient"/> class.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="firstName">The first name.</param>
        /// <param name="lastName">The last name.</param>
        /// <param name="isBoy">if set to <c>true</c> [is boy].</param>
        /// <param name="birthday">The birthday.</param>
        /// <param name="ratios">The ratios.</param>
        /// <param name="contacts">The contacts.</param>
        public Patient(Model model, string firstName, string lastName, bool isBoy, DateTime birthday, Ratios ratios, Contacts contacts)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Birthday = birthday;
            this.Ratios = ratios;
            this.Contacts = contacts;
            this.Model = model;
            this.IsBoy = isBoy;
        }
    }
}
