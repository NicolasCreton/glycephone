﻿using GlycePhoneModel;
using GlycePhonePatient;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace GlycePhone
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ProfilPage : GlycePhonePage
    {

        private StackPanel customFoodPanel;
        private LinkedList<CustomFoodPanel> customFoodPanels = new LinkedList<CustomFoodPanel>();
        private const string customFoodNameLabel = "Nom de l'aliment";
        private const string customFoodGlucidesLabel = "Glucides par gramme";
        private TextBox foodElementNameTextBox;
        private TextBox foodElementGlucidesTextBox;
        private StackPanel firstNameContainer;
        private StackPanel lastNameContainer;
        private StackPanel birthdayContainer;
        private Button modificationButton;
        public DateTime DateBegin { get; set; }
        public DateTime DateEnd { get; set; }
        public ProfilPage()
        {
            this.InitializeComponent();
            this.NeedSaving = false;
            this.IsRotation = false;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// This class represents the profil page. It is displayed with a hub where the user can set his profil and change parameter
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.DateEnd = DateTime.Now;
        }

        /// <summary>
        /// Handles the Tapped event of the SeeCustomHistoryButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TappedRoutedEventArgs"/> instance containing the event data.</param>
        private void SeeCustomHistoryButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if ((this.DateBegin < DateTime.Now) && (this.DateEnd < DateTime.Now) && (this.DateBegin < this.DateEnd.Date))
            {
                List<DateTime> objDate = new List<DateTime>();
                objDate.Add(this.DateBegin);
                objDate.Add(this.DateEnd);
                this.Navigate(typeof(MealHistoryCustomScreen), objDate);
            }
        }

        /// <summary>
        /// Handles the DateChanged event of the DateBegin control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DatePickerValueChangedEventArgs"/> instance containing the event data.</param>
        private void DateBegin_DateChanged(object sender, DatePickerValueChangedEventArgs e)
        {
            this.DateBegin = e.NewDate.DateTime;
            Debug.WriteLine(this.DateBegin.ToString());
        }

        /// <summary>
        /// Handles the DateChanged event of the DateFinish control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DatePickerValueChangedEventArgs"/> instance containing the event data.</param>
        private void DateFinish_DateChanged(object sender, DatePickerValueChangedEventArgs e)
        {
            this.DateEnd = e.NewDate.DateTime;
            Debug.WriteLine(this.DateEnd.ToString());
        }

        /// <summary>
        /// Handles the Tapped event of the ChangeIdentity control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TappedRoutedEventArgs"/> instance containing the event data.</param>
        private void ChangeIdentity_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (IsModification)
            {
                SaveModifications();
            }
            IsModification = !IsModification;
        }

        /// <summary>
        /// Saves the modifications.
        /// </summary>
        private void SaveModifications()
        {
            foreach (var control in this.firstNameContainer.Children)
            {
                if (control is TextBox)
                {
                    string name = ((TextBox)control).Text;
                    this.Model.Patient.FirstName = name != null && name != "" ? name : this.Model.Patient.FirstName;
                }
            }
            foreach (var control in this.lastNameContainer.Children)
            {
                if (control is TextBox)
                {
                    string name = ((TextBox)control).Text;
                    this.Model.Patient.LastName = name != null && name != "" ? name : this.Model.Patient.LastName;
                }
            }
            foreach (var control in this.birthdayContainer.Children)
            {
                if (control is DatePicker)
                {
                    DateTime birthday = ((DatePicker)control).Date.DateTime;
                    this.Model.Patient.Birthday = birthday;
                }
            }
            this.Model.Save();
        }

        private bool isModification = false;
        /// <summary>
        /// Gets or sets a value indicating whether this instance can be modified.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is modification; otherwise, <c>false</c>.
        /// </value>
        private bool IsModification
        {
            get
            {
                return isModification;
            }
            set
            {
                isModification = value;
                if (value)
                {
                    EnableModification();
                }
                else
                {
                    DisableModification();
                }
            }
        }

        /// <summary>
        /// Clears the information containers.
        /// </summary>
        private void ClearInfoContainers()
        {
            this.firstNameContainer.Children.Clear();
            this.lastNameContainer.Children.Clear();
            this.birthdayContainer.Children.Clear();
        }


        /// <summary>
        /// Disables the modification.
        /// </summary>
        private void DisableModification()
        {
            ClearInfoContainers();
            this.firstNameContainer.Children.Add(new TextBlock()
            {
                HorizontalAlignment = HorizontalAlignment.Left,
                TextWrapping = TextWrapping.Wrap,
                Text = Model.Patient.FirstName,
                VerticalAlignment = VerticalAlignment.Top,
                FontSize = 24
            });
            this.lastNameContainer.Children.Add(new TextBlock()
            {
                HorizontalAlignment = HorizontalAlignment.Left,
                TextWrapping = TextWrapping.Wrap,
                Text = Model.Patient.LastName,
                VerticalAlignment = VerticalAlignment.Top,
                FontSize = 24
            });

            this.birthdayContainer.Children.Add(new TextBlock()
            {
                HorizontalAlignment = HorizontalAlignment.Left,
                TextWrapping = TextWrapping.Wrap,
                Text = Model.Patient.BirthFormat,
                VerticalAlignment = VerticalAlignment.Top,
                FontSize = 24
            }
            );

            this.modificationButton.Content = "Modifier";
        }

        /// <summary>
        /// Enables the modification.
        /// </summary>
        private void EnableModification()
        {
            ClearInfoContainers();
            this.firstNameContainer.Children.Add(new TextBox()
            {
                Text = Model.Patient.FirstName,
            });
            this.lastNameContainer.Children.Add(new TextBox()
            {
                Text = Model.Patient.LastName,
            });

            this.birthdayContainer.Children.Add(new DatePicker()
            {
                Date = Model.Patient.Birthday.Date
            }
            );

            this.modificationButton.Content = "Valider";
        }

        /// <summary>
        /// Handles the Tapped event of the ChangeRatiosButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TappedRoutedEventArgs"/> instance containing the event data.</param>
        private void ChangeRatiosButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.Navigate(typeof(InscriptionsPageMeals));
        }

        /// <summary>
        /// Handles the Tapped event of the ChangeContactsButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TappedRoutedEventArgs"/> instance containing the event data.</param>
        private void ChangeContactsButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.Navigate(typeof(InscriptionPageContacts));
        }

        /// <summary>
        /// Handles the Tapped event of the MakePhoneCallButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TappedRoutedEventArgs"/> instance containing the event data.</param>
        private void MakePhoneCallButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            SelectPhone();
        }

        /// <summary>
        /// Handles the Tapped event of the SendSMSButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TappedRoutedEventArgs"/> instance containing the event data.</param>
        private void SendSMSButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            SelectSMS();
        }

        /// <summary>
        /// Handles the Checked event of the MakePhoneCallRadioButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void MakePhoneCallRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            SelectPhone();
        }

        /// <summary>
        /// Handles the Checked event of the SendSMSRadioButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void SendSMSRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            SelectSMS();
        }

        /// <summary>
        /// Selects the phone.
        /// </summary>
        private void SelectPhone()
        {
            Select(EmergencyAction.Phone);
        }

        /// <summary>
        /// Selects the SMS.
        /// </summary>
        private void SelectSMS()
        {
            Select(EmergencyAction.Message);
        }

        /// <summary>
        /// Selects the specified action.
        /// </summary>
        /// <param name="action">The action.</param>
        private void Select(EmergencyAction action)
        {
            this.Model.Patient.Contacts.EmergencyAction = action;
            this.Model.Save();
        }


        /// <summary>
        /// Handles the Loaded event of the CustomFoodPanel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void CustomFoodPanel_Loaded(object sender, RoutedEventArgs e)
        {
            this.customFoodPanel = (StackPanel)sender;
            RefreshCustomFoodList();
        }

        /// <summary>
        /// Refreshes the custom food list.
        /// </summary>
        private void RefreshCustomFoodList()
        {
            this.customFoodPanel.Children.Clear();
            foreach (var foodElement in this.Model.CustomFoodList)
            {
                CustomFoodPanel customFoodPanel = new CustomFoodPanel(foodElement, this);
                this.customFoodPanel.Children.Add(customFoodPanel.Container);
                customFoodPanels.AddLast(customFoodPanel);
            }
        }

        

        /// <summary>
        /// Handles the Click event of the NewCustomFoodElement control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void NewCustomFoodElement_Click(object sender, RoutedEventArgs e)
        {
            AddFoodElement();
        }

        /// <summary>
        /// Adds the food element.
        /// </summary>
        private void AddFoodElement()
        {
            try
            {
                FoodElement foodElement = new FoodElement("Custom", foodElementNameTextBox.Text, ((float)Convert.ToDouble(foodElementGlucidesTextBox.Text)));
                CustomFoodPanel customFoodPanel = new CustomFoodPanel(foodElement, this);
                this.customFoodPanel.Children.Add(customFoodPanel.Container);
                customFoodPanels.AddLast(customFoodPanel);
                this.Model.CustomFoodList.AddLast(foodElement);
                this.foodElementNameTextBox.Text = customFoodNameLabel;
                this.foodElementGlucidesTextBox.Text = customFoodGlucidesLabel;
                this.Model.Save();
            }
            catch (FormatException e)
            {

            }
        }

        /// <summary>
        /// Handles the Loaded event of the FoodElementGlucides control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void FoodElementGlucides_Loaded(object sender, RoutedEventArgs e)
        {
            foodElementGlucidesTextBox = (TextBox)sender;

        }

        /// <summary>
        /// Handles the Loaded event of the FoodElementName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void FoodElementName_Loaded(object sender, RoutedEventArgs e)
        {
            this.foodElementNameTextBox = (TextBox)sender;
        }

        /// <summary>
        /// Handles the Loaded event of the FirstNameContainer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void FirstNameContainer_Loaded(object sender, RoutedEventArgs e)
        {
            this.firstNameContainer = (StackPanel)sender;
        }

        /// <summary>
        /// Handles the Loaded event of the ChangeIdentity control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void ChangeIdentity_Loaded(object sender, RoutedEventArgs e)
        {
            this.modificationButton = (Button)sender;
            modificationButton.FontFamily = new FontFamily("/Fonts/Simpsonfont.ttf#Simpsonfont");
        }

        /// <summary>
        /// Handles the Loaded event of the LastNameContainer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void LastNameContainer_Loaded(object sender, RoutedEventArgs e)
        {
            this.lastNameContainer = (StackPanel)sender;
        }

        /// <summary>
        /// Handles the Loaded event of the BirthdayContainer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void BirthdayContainer_Loaded(object sender, RoutedEventArgs e)
        {
            this.birthdayContainer = (StackPanel)sender;
        }


        /// <summary>
        /// Removes the food element.
        /// </summary>
        /// <param name="CustomFood">The custom food.</param>
        public void RemoveFoodElement(FoodElement CustomFood)
        {
            this.Model.CustomFoodList.Remove(CustomFood);
            this.Model.Save();
            RefreshCustomFoodList();
        }

        /// <summary>
        /// Handles the GotFocus event of the CustomFoodName_TextBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void CustomFoodName_TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox t = (TextBox)sender;
            if (t.Text == customFoodNameLabel)
            {
                t.Text = "";
            }
        }

        /// <summary>
        /// Handles the GotFocus event of the CustomFoodGlucides_TextBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void CustomFoodGlucides_TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox t = (TextBox)sender;
            if (t.Text == customFoodGlucidesLabel)
            {
                t.Text = "";
            }
        }

        /// <summary>
        /// Handles the LostFocus event of the CustomFoodName_TextBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void CustomFoodName_TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox t = (TextBox)sender;
            if (t.Text == "")
            {
                t.Text = customFoodNameLabel;
            }
        }

        /// <summary>
        /// Handles the LostFocus event of the CustomFoodGlucides_TextBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void CustomFoodGlucides_TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox t = (TextBox)sender;
            if (t.Text == "")
            {
                t.Text = customFoodGlucidesLabel;
            }
        }

        /// <summary>
        /// Handles the KeyDown event of the CustomFoodName_TextBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyRoutedEventArgs"/> instance containing the event data.</param>
        private void CustomFoodName_TextBox_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                this.foodElementGlucidesTextBox.Focus(FocusState.Keyboard);
            }
        }

        /// <summary>
        /// Handles the KeyDown event of the CustomFoodGlucides_TextBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyRoutedEventArgs"/> instance containing the event data.</param>
        private void CustomFoodGlucides_TextBox_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                this.LoseFocus(this.foodElementGlucidesTextBox);
                this.AddFoodElement();
            }
        }

        /// <summary>
        /// Goes back to the previous page
        /// </summary>
        public override void GoBack()
        {
            this.Navigate(typeof(MainMenuScreen));
        }

        /// <summary>
        /// Handles the Loaded event of the SendSMSRadioButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void SendSMSRadioButton_Loaded(object sender, RoutedEventArgs e)
        {
            RadioButton radio = (RadioButton)sender;
            radio.IsChecked = (this.Model.Patient.Contacts.EmergencyAction == EmergencyAction.Message);
        }

        /// <summary>
        /// Handles the Loaded event of the MakePhoneCallRadioButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void MakePhoneCallRadioButton_Loaded(object sender, RoutedEventArgs e)
        {
            RadioButton radio = (RadioButton)sender;
            radio.IsChecked = (this.Model.Patient.Contacts.EmergencyAction == EmergencyAction.Phone);
        }
    }
}
