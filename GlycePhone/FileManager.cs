﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Windows.Foundation;
using Windows.Storage;


namespace GlycePhoneModel
{
    /// <summary>
    /// Load and save a file from the phone storage folder
    /// </summary>
    public class FileManager
    {
        public static readonly StorageFolder FOLDER = ApplicationData.Current.LocalFolder;

        private static bool isLoading = false;
        /// <summary>
        /// Gets or sets a value indicating whether a file is loading.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is loading; otherwise, <c>false</c>.
        /// </value>
        public static bool IsLoading
        {
            get
            {
                return isLoading;
            }
            set
            {
                isLoading = value;
            }
        }

        private static object loadedObject = null;
        public static object LoadedObject
        {
            get
            {
                return loadedObject;
            }
            set
            {
                loadedObject = value;
            }
        }

        /// <summary>
        /// Starts the loading.
        /// </summary>
        private static void StartLoading()
        {
            isLoading = true;
            loadedObject = null;
        }

        /// <summary>
        /// Stops the loading.
        /// </summary>
        /// <param name="obj">The object.</param>
        private static void StopLoading(object obj)
        {
            loadedObject = obj;
            isLoading = false;
        }

        /// <summary>
        /// Loads the XML file asynchronously.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        public static async Task LoadXMLAsync<T>(string fileName)
        {
            StartLoading();
            string content = String.Empty;
            object obj = null;
            var serializer = new DataContractSerializer(typeof(T));
            try
            {
                using (var stream = await FOLDER.OpenStreamForReadAsync(fileName))
                {
                    obj = serializer.ReadObject(stream);
                }
            }
            catch
            {
                obj = null;
            }
            StopLoading(obj);
        }

        /// <summary>
        /// Loads the json file asynchronously.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        public static async Task LoadJSONAsync<T>(string fileName)
        {
            StartLoading();
            
            string content = String.Empty;
            object obj = null;
            var serializer = new DataContractJsonSerializer(typeof(T));
            try
            {
                using (var stream = await FOLDER.OpenStreamForReadAsync(fileName))
                {
                    obj = serializer.ReadObject(stream);
                }
            }
            catch
            {
                obj = null;
            }
            ReadJSONAsync<T>(fileName);
            StopLoading(obj);
        }

        /// <summary>
        /// Reads the json file asynchronously.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        public static async Task ReadJSONAsync<T>(string fileName)
        {
            string content = String.Empty;
            var myStream = await FOLDER.OpenStreamForReadAsync(fileName);
            using (StreamReader reader = new StreamReader(myStream))
            {
                content = await reader.ReadToEndAsync();
            }
            Debug.WriteLine(content);
        }

        /// <summary>
        /// Saves the json file asynchronously.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj">The object.</param>
        /// <param name="fileName">Name of the file.</param>
        public async static void SaveJSONAsync<T>(T obj, string fileName)
        {
            try
            {
                var serializer = new DataContractJsonSerializer(typeof(T));
                using (var stream = await FOLDER.OpenStreamForWriteAsync(fileName, CreationCollisionOption.ReplaceExisting))
                {
                    serializer.WriteObject(stream, obj);
                    Debug.WriteLine("Saved !");
                }
            }
            catch
            {
                Debug.WriteLine("Couldn't save..."); //In case of an error, we cancel the operation
            }
        }

        /// <summary>
        /// Saves the XML file asynchronously.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj">The object.</param>
        /// <param name="fileName">Name of the file.</param>
        public async static void SaveXMLAsync<T>(T obj, string fileName)
        {
            try
            {
                var serializer = new DataContractSerializer(typeof(T));
                using (var stream = await FOLDER.OpenStreamForWriteAsync(fileName, CreationCollisionOption.ReplaceExisting))
                {
                    serializer.WriteObject(stream, obj);
                    Debug.WriteLine("Saved !");
                }
            }
            catch
            {
                Debug.WriteLine("Couldn't save..."); //In case of an error, we cancel the operation
            }
        }

    }
}
