﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GlycePhonePatient
{
    [DataContract]
    public class Contacts
    {
        /// <summary>
        /// Gets or sets the people in the emergency contacts list
        /// </summary>
        /// <value>
        /// The people.
        /// </value>
        [DataMember]
        public List<Contact> People { get; set; }

        /// <summary>
        /// Gets or sets the emergency action (Message or phone)
        /// </summary>
        /// <value>
        /// The emergency action.
        /// </value>
        [DataMember]
        public EmergencyAction EmergencyAction { get; set; }

        public Contacts()
        {
            this.People = new List<Contact>();
            this.EmergencyAction = EmergencyAction.Message;
        }

        /// <summary>
        /// Adds the specified contact.
        /// </summary>
        /// <param name="contact">The contact.</param>
        public void Add(Contact contact)
        {
            this.People.Add(contact);
        }

        /// <summary>
        /// Removes the specified contact.
        /// </summary>
        /// <param name="contact">The contact.</param>
        public void Remove(Contact contact)
        {
            this.People.Remove(contact);
        }

        /// <summary>
        /// Gets the enumerator.
        /// </summary>
        /// <returns></returns>
        public List<Contact>.Enumerator GetEnumerator()
        {
            return People.GetEnumerator();
        }

        /// <summary>
        /// Gets or sets the <see cref="Contact"/> with the specified i.
        /// </summary>
        /// <value>
        /// The <see cref="Contact"/>.
        /// </value>
        /// <param name="i">The i.</param>
        /// <returns></returns>
        public Contact this[int i]
        {
            get
            {
                return People[i];
            }

            set
            {
                People[i] = value;
            }
        }

    }
}
