﻿using GlycePhone;
using GlycePhoneModel;
using GlycePhoneViewInformation;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Graphics.Display;
using Windows.Phone.UI.Input;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace GlycePhone
{
    public class GlycePhonePage : Page
    {

        /// <summary>
        /// Gets or sets the information of the app.
        /// </summary>
        /// <value>
        /// The information.
        /// </value>
        public AppInfo Information { get; set; }
        /// <summary>
        /// Gets or sets the model.
        /// </summary>
        /// <value>
        /// The model.
        /// </value>
        public Model Model { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the model needs to be saved when leaving the page.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [need saving]; otherwise, <c>false</c>.
        /// </value>
        public bool NeedSaving { get; set; }

        /// <summary>
        /// The current page
        /// </summary>
        private static GlycePhonePage currentPage;

        private static bool backPressedHandlerAdded = false;

        /// <summary>
        /// Specifies whether the phone can be rotated or can only be displayed in portrait mode
        /// </summary>
        private bool isRotation;

        /// <summary>
        /// Specifies whether the phone can be rotated or can only be displayed in portrait mode
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance can be rotated; otherwise, <c>false</c>.
        /// </value>
        public bool IsRotation
        {
            get 
            { 
                return isRotation; 
            }
            set { 
                isRotation = value;
                if (isRotation)
                {
                    DisplayInformation.AutoRotationPreferences = DisplayOrientations.Portrait | DisplayOrientations.Landscape | DisplayOrientations.LandscapeFlipped;
                }
                else
                {
                    DisplayInformation.AutoRotationPreferences = DisplayOrientations.Portrait;
                }

            }
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="GlycePhonePage"/> class.
        /// </summary>
        public GlycePhonePage()
        {
            this.Information = (App.Current as App).Information;
            this.Model = this.Information.Model;
            this.DataContext = this.Information;
            this.NeedSaving = false;
            this.isRotation = true;
            this.InitHardwareButtons();
            currentPage = this;
        }

        /// <summary>
        /// Initializes the hardware buttons.
        /// </summary>
        private void InitHardwareButtons()
        {
            if (!backPressedHandlerAdded)
            {
                backPressedHandlerAdded = true;
                HardwareButtons.BackPressed += HardwareButtons_BackPressed;
                HardwareButtons.CameraPressed += HardwareButtons_CameraPressed;
            }
        }

        /// <summary>
        /// Handles the CameraPressed event of the HardwareButtons control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="CameraEventArgs"/> instance containing the event data.</param>
        void HardwareButtons_CameraPressed(object sender, CameraEventArgs e)
        {
            currentPage.Navigate(typeof(MainMenuScreen));
        }

        /// <summary>
        /// Handles the BackPressed event of the HardwareButtons control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="BackPressedEventArgs"/> instance containing the event data.</param>
        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            e.Handled = true;
            currentPage.GoBack();
        }

        /// <summary>
        /// Navigates to the specified page type
        /// </summary>
        /// <param name="t">The t.</param>
        /// <param name="args">The arguments.</param>
        public void Navigate(Type t, object args = null)
        {
            if (currentPage.NeedSaving)
            {
                this.Model.Save();
            }
            Frame.Navigate(t, args);
        }

        /// <summary>
        /// Goes back to the previous page
        /// </summary>
        public virtual void GoBack()
        { 
            if (Frame.CanGoBack)
            {
                if (currentPage.NeedSaving)
                {
                    Debug.WriteLine(currentPage);
                    this.Model.Save();
                }
                Frame.GoBack();
            }
        }

        /// <summary>
        /// The specified control loses the focus.
        /// </summary>
        /// <param name="control">The control.</param>
        public void LoseFocus(Control control)
        {
            bool isTabStop = control.IsTabStop;
            control.IsTabStop = false;
            control.IsEnabled = false;
            control.IsEnabled = true;
            control.IsTabStop = isTabStop;
            Windows.UI.ViewManagement.InputPane.GetForCurrentView().TryHide();
        }
    }
}
