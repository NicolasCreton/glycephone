﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GlycePhoneUtil
{
    [DataContract]
    public class Person
    {
        private const string defaultFirstName = "Prénom";
        private const string defaultLastName = "Nom";

        private string firstName;

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>
        /// The first name.
        /// </value>
        [DataMember]
        public virtual string FirstName
        {
            get { return firstName; }
            set
            {
                if (value == null)
                {
                    firstName = defaultFirstName;
                }
                else {
                    firstName = value; 
                }
            }
        }

        private string lastName;

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>
        /// The last name.
        /// </value>
        [DataMember]
        public virtual string LastName
        {
            get { return lastName; }
            set
            {
                if (value == null)
                {
                    lastName = defaultLastName;
                }
                else
                {
                    lastName = value;
                }
            }
        }

        /// <summary>
        /// Gets the full name.
        /// </summary>
        /// <value>
        /// The full name.
        /// </value>
        [IgnoreDataMember]
        public virtual string FullName
        {
            get
            {
                return string.Format("{0} {1}", FirstName, LastName);
            }
        }
        
        
    }
}
