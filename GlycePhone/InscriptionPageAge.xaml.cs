﻿using GlycePhoneModel;
using GlycePhoneViewInformation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace GlycePhone
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class InscriptionPageAge : GlycePhonePage
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="InscriptionPageAge"/> class.
        /// </summary>
        public InscriptionPageAge()
        {
            this.InitializeComponent();
            this.NeedSaving = true;
            this.IsRotation = true;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            BirthDate.Date = Model.Patient.Birthday;
            this.WelcomeText.Text = string.Format("Bienvenue {0} !", this.Model.Patient.FirstName);
        }

        private void ValidateButton_Click(object sender, RoutedEventArgs e)
        {
            Model.Patient.Birthday = BirthDate.Date.DateTime;
            this.Navigate(typeof(InscriptionsPageMeals));
        }

        private void GoBackButton_Click(object sender, RoutedEventArgs e)
        {
            this.GoBack();
        }

    }
}
