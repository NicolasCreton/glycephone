﻿using GlycePhoneModel;
using GlycePhonePatient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GlycePhone
{
    [DataContract] //Serializable
    public class InsulineCalculus
    {
        /// <summary>
        /// Gets or sets the model.
        /// </summary>
        /// <value>
        /// The model.
        /// </value>
        public Model Model { get; set; }

        /// <summary>
        /// Gets or sets the meal.
        /// </summary>
        /// <value>
        /// The meal.
        /// </value>
        [DataMember]
        public MealType Meal { get; set; }

        /// <summary>
        /// Gets or sets the insuline dose.
        /// </summary>
        /// <value>
        /// The insuline dose.
        /// </value>
        [DataMember]
        public float InsulineDose { get; set; }

        /// <summary>
        /// Gets or sets the food list.
        /// </summary>
        /// <value>
        /// The food list.
        /// </value>
        public List<FoodEaten> FoodList { get; set; }

        /// <summary>
        /// Gets or sets the total glucides.
        /// </summary>
        /// <value>
        /// The total glucides.
        /// </value>
        [DataMember]
        public float TotalGlucides { get; set; }

        /// <summary>
        /// Gets or sets the date hour.
        /// </summary>
        /// <value>
        /// The date hour.
        /// </value>
        [DataMember]
        public DateTime DateHour { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="InsulineCalculus"/> class.
        /// This class do the calculus of insulina to take after the meal
        /// </summary>
        /// <param name="model">The model.</param>
        public InsulineCalculus(Model model)
        {
            this.Model = model;
            this.FoodList = new List<FoodEaten>();
            this.InsulineDose = 0;
            this.DateHour = DateTime.Now;
        }

        /// <summary>
        /// Eatens the list to string list.
        /// </summary>
        /// <returns></returns>
        public List<string> EatenListToStringList()
        {
            List<string> eatenListString = new List<string>();
            foreach (FoodEaten fe in this.FoodList)
            {
                eatenListString.Add(fe.ToString());
            }
            return eatenListString;
        }

        /// <summary>
        /// Dose calculus.
        /// </summary>
        /// <returns>dose of insulina</returns>
        public float DoseCalculus()
        {
            return (this.TotalGlucides) * this.Model.Patient.Ratios[this.Meal];
        }

        public override string ToString()
        {
            return Meal.WrittenName + " : " + DateHour.ToString() + " : " + TotalGlucides.ToString() + " grammes de glucides";
        }

    }
}
