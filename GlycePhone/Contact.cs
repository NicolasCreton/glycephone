﻿using GlycePhoneUtil;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GlycePhonePatient
{
    [DataContract]
    public class Contact : Person
    {
        /// <summary>
        /// Gets or sets the phone number.
        /// </summary>
        /// <value>
        /// The phone number.
        /// </value>
        [DataMember]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Contact"/> class.
        /// </summary>
        /// <param name="firstName">The first name.</param>
        /// <param name="lastName">The last name.</param>
        /// <param name="phoneNumber">The phone number.</param>
        public Contact(string firstName, string lastName, string phoneNumber)
        {
            this.PhoneNumber = phoneNumber;
            this.FirstName = firstName;
            this.LastName = lastName;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Contact"/> class. The last name is set to empty. This is intended to use as a nickname
        /// </summary>
        /// <param name="firstName">The first name.</param>
        /// <param name="phoneNumber">The phone number.</param>
        public Contact(string firstName, string phoneNumber)
            : this(firstName, String.Empty, phoneNumber)
        {

        }

    }
}
