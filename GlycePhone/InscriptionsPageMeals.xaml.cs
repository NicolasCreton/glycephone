﻿using GlycePhoneModel;
using GlycePhonePatient;
using GlycePhoneViewInformation;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace GlycePhone
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class InscriptionsPageMeals : GlycePhonePage
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="InscriptionsPageMeals"/> class.
        /// </summary>
        public InscriptionsPageMeals()
        {
            this.InitializeComponent();
            this.NeedSaving = true;
            this.IsRotation = true;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            foreach (var mealType in Model.DefaultMealTypes.Types)
            {
                if (mealType is MealType)
                {
                    CheckBox meal = new CheckBox();
                    meal.FontFamily = new FontFamily("/Fonts/Simpsonfont.ttf#Simpsonfont");
                    meal.Content = mealType.WrittenName;
                    meal.Name = mealType.Name;
                    Meals.Children.Add(meal);
                }
            }
            foreach (var ratio in Model.Patient.Ratios.Values)
            {
                if (Model.DefaultMealTypes.Find(ratio.MealType.Name) != null)
                {
                    foreach (var meal in Meals.Children)
                    {
                        if (meal is CheckBox && ((CheckBox)meal).Content.ToString() == ratio.MealType.WrittenName)
                        {

                            ((CheckBox)meal).IsChecked = true;
                        }
                    }
                }
                else
                {
                    CheckBox newMeal = new CheckBox();
                    newMeal.FontFamily = new FontFamily("/Fonts/Simpsonfont.ttf#Simpsonfont");
                    newMeal.Content = ratio.MealType.WrittenName;
                    newMeal.Name = ratio.MealType.Name;
                    newMeal.IsChecked = true;
                    Meals.Children.Add(newMeal);
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the NewMealButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void NewMealButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (var meal in Meals.Children)
            {
                if (meal is CheckBox)
                {
                    CheckBox chbxMeal = ((CheckBox)meal);
                    chbxMeal.FontFamily = new FontFamily("/Fonts/Simpsonfont.ttf#Simpsonfont");
                    MealType mealType = Model.DefaultMealTypes.Find(chbxMeal.Name);
                    if (chbxMeal.IsChecked.Value)
                    {
                        if (mealType == null)
                        {
                            mealType = new MealType(chbxMeal.Name);
                        }
                        this.Model.Patient.Ratios[mealType] = Ratio.NOT_SPECIFIED;
                    }
                    else
                    {
                        this.Model.Patient.Ratios[(chbxMeal.Name)] = Ratio.NULL;
                    }
                }
            }
            this.Navigate(typeof(InscriptionsPageRatios));
        }

        /// <summary>
        /// Handles the Click event of the GoBackButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void GoBackButton_Click(object sender, RoutedEventArgs e)
        {
            this.GoBack();
        }

        /// <summary>
        /// Handles the Tapped event of the NomRepas control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void NomRepas_Tapped(object sender, RoutedEventArgs e)
        {
            this.repas.Focus(FocusState.Programmatic);
        }

        /// <summary>
        /// Determines whether [contains meal type] [the specified meal name].
        /// </summary>
        /// <param name="mealName">Name of the meal.</param>
        /// <returns></returns>
        private bool ContainsMealType(string mealName)
        {
            if (this.Model.DefaultMealTypes.Find(mealName) != null)
            {
                return false;
            }
            foreach (var meal in Meals.Children)
            {
                if (meal is CheckBox)
                {
                    CheckBox chbxMeal = ((CheckBox)meal);
                    if (chbxMeal.Name == mealName)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Handles the Click event of the AddMeal control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void AddMeal_Click(object sender, RoutedEventArgs e)
        {
            ValidateMeal();
        }

        /// <summary>
        /// Handles the KeyDown event of the repas control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyRoutedEventArgs"/> instance containing the event data.</param>
        private void repas_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                ValidateMeal();
            }
        }

        /// <summary>
        /// Validates the meal.
        /// </summary>
        private void ValidateMeal()
        {
            if (this.repas.Text != "" && !ContainsMealType(this.repas.Text))
            {
                CheckBox meal = new CheckBox();
                meal.Name = this.repas.Text;
                meal.Content = this.repas.Text;
                meal.IsChecked = true;
                Meals.Children.Add(meal);
            }
            repas.Text = "";
        }

    }
}
