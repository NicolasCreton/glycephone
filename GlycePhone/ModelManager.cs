﻿using GlycePhoneObserver;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Windows.Foundation;
using Windows.Storage;

namespace GlycePhoneModel
{
    public class ModelManager : IObserver
    {
        private static readonly string MODEL = "model";
        private static readonly string XML_EXTENSION = ".xml";
        private static readonly string JSON_EXTENSION = ".json";
        private static readonly string CURRENT_EXTENSION = JSON_EXTENSION;

        private static readonly string MODEL_XML_FILENAME = MODEL + XML_EXTENSION;
        private static readonly string MODEL_JSON_FILENAME = MODEL + JSON_EXTENSION;

        private static readonly StorageFolder FOLDER = FileManager.FOLDER;

        private Model model;

        private static ModelManager modelManager = null;

        public Model Model
        {
            get { return model; }
            set
            {
                model = value;
                model.AddObserver(this);
            }
        }


        private ModelManager(ObservableModel model)
        {
            this.Model = (Model)model;
        }

        //Singleton
        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        public static ModelManager GetInstance(ObservableModel model)
        {
            if (modelManager == null)
            {
                modelManager = new ModelManager(model);
            }
            return modelManager;
        }

        /// <summary>
        /// Saves the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <exception cref="Exception">The asked extension is not available</exception>
        public static void Save(Model model)
        {
            if (CURRENT_EXTENSION == JSON_EXTENSION)
            {
                SaveJSON(model);
            }
            else if (CURRENT_EXTENSION == JSON_EXTENSION)
            {
                SaveXML(model);
            }
            else
            {
                throw new Exception("The asked extension is not available");
            }
        }

        /// <summary>
        /// Saves the specified model in XML format.
        /// </summary>
        /// <param name="model">The model.</param>
        private static void SaveXML(Model model)
        {
            FileManager.SaveXMLAsync<Model>(model, MODEL_XML_FILENAME);
        }

        /// <summary>
        /// Saves the specified model in JSON format.
        /// </summary>
        /// <param name="model">The model.</param>
        private static void SaveJSON(Model model)
        {
            FileManager.SaveJSONAsync<Model>(model, MODEL_JSON_FILENAME);
        }

        /// <summary>
        /// Loads the model.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception">The asked extension is not available</exception>
        public static Model LoadModel()
        {
            if (CURRENT_EXTENSION == JSON_EXTENSION)
            {
                LoadModelJSON();
            }
            else if (CURRENT_EXTENSION == JSON_EXTENSION)
            {
                LoadModelXML();
            }
            else
            {
                throw new Exception("The asked extension is not available");
            }

            while (FileManager.IsLoading)
            {
                //Wait
            }

            object obj = FileManager.LoadedObject;
            if (obj != null && obj is Model)
            {
                return (Model)obj;
            }
            return null;
        }

        /// <summary>
        /// Loads the model XML formatted file.
        /// </summary>
        private static void LoadModelXML()
        {
            FileManager.LoadXMLAsync<Model>(MODEL_XML_FILENAME);
        }

        /// <summary>
        /// Loads the model json formatted file.
        /// </summary>
        private static void LoadModelJSON()
        {
            FileManager.LoadJSONAsync<Model>(MODEL_JSON_FILENAME);
        }




        /// <summary>
        /// Updates the specified observable.
        /// </summary>
        /// <param name="observable">The observable.</param>
        public void Update(IObservable observable)
        {
            if (observable == this.Model && this.Model.IsSave)
            {
                Save(this.Model);
            }
        }
    }
}