﻿using GlycePhone;
using GlycePhoneModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace GlycePhone
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class EndOfTheMealScreen : GlycePhonePage
    {
        public EndOfTheMealScreen()
        {
            this.InitializeComponent();
            this.IsRotation = true;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// In this frame we will display the food eaten and the insulina to take after the meal
        /// The caclulation is made in InsulineCalculus
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.PatientNameBlock.Text = "Bien joué " + this.Model.Patient.FirstName;
            this.EatenList.ItemsSource = this.Model.Calculus.EatenListToStringList();
            this.GlucideTotalBox.Text = this.Model.Calculus.TotalGlucides.ToString("0.0") + " grammes de glucides";
            this.Model.Calculus.InsulineDose = this.Model.Calculus.DoseCalculus();
            this.DoseText.Text = this.Model.Calculus.InsulineDose.ToString("0.0");
            this.Model.MealHistoryList.AddLast(this.Model.Calculus);
        }

        /// <summary>
        /// Handles the Tapped event of the MainMenuButton control. Goes back to the Home page
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TappedRoutedEventArgs"/> instance containing the event data.</param>
        private void MainMenuButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.Navigate(typeof(MainMenuScreen));
        }
    }
}
