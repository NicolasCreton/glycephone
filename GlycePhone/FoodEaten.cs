﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlycePhone
{
    public class FoodEaten
    {
        /// <summary>
        /// Gets or sets the food element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public FoodElement Element { get; set; }
        /// <summary>
        /// Gets or sets the quantity.
        /// </summary>
        /// <value>
        /// The quantity.
        /// </value>
        public float Quantity { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="FoodEaten"/> class.
        /// This class represents a food eaten with its quantity in grammes
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="quantity">The quantity.</param>
        public FoodEaten(FoodElement element, float quantity)
        {
            this.Element = element;
            this.Quantity = quantity;
        }

        public override string ToString()
        {
            return Element.Name + " : " + Quantity.ToString() + " grammes";
        }
    }

}
