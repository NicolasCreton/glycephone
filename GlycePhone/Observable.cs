﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GlycePhoneObserver
{
    [DataContract]
    public class Observable:IObservable
    {
        /// <summary>
        /// The observers
        /// </summary>
        private List<IObserver> observers = new List<IObserver>();

        /// <summary>
        /// Adds the observer.
        /// </summary>
        /// <param name="observer">The observer.</param>
        public void AddObserver(IObserver observer)
        {
            if (observers == null)
            {
                observers = new List<IObserver>(1);
            }
            observers.Add(observer);
        }

        /// <summary>
        /// Removes the observer.
        /// </summary>
        /// <param name="observer">The observer.</param>
        public void RemoveObserver(IObserver observer)
        {
            observers.Remove(observer);
        }

        /// <summary>
        /// Notifies the observers
        /// </summary>
        public void Notify()
        {
            if (observers != null)
            {
                foreach (IObserver observer in observers)
                {
                    observer.Update(this);
                }
            }
        }
    }
}
