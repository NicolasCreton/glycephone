﻿using GlycePhone;
using GlycePhoneObserver;
using GlycePhonePatient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace GlycePhoneModel
{
    [DataContract]
    public class Model : ObservableModel
    {
        /// <summary>
        /// The default meal types
        /// </summary>
        [DataMember]
        public DefaultMealTypes DefaultMealTypes;

        [DataMember]
        public bool Registered { get; set; }

        /// <summary>
        /// Gets or sets the calculus.
        /// </summary>
        /// <value>
        /// The calculus.
        /// </value>
        [DataMember]
        public InsulineCalculus Calculus { get; set; }

        /// <summary>
        /// Gets or sets the meal history list.
        /// </summary>
        /// <value>
        /// The meal history list.
        /// </value>
        [DataMember]
        public LinkedList<InsulineCalculus> MealHistoryList { get; set; }

        /// <summary>
        /// Gets or sets the food list.
        /// </summary>
        /// <value>
        /// The food list.
        /// </value>
        [IgnoreDataMember]
        public LinkedList<FoodElement> FoodList { get; set; }

        /// <summary>
        /// Gets or sets the custom food list.
        /// </summary>
        /// <value>
        /// The custom food list.
        /// </value>
        [DataMember]
        public LinkedList<FoodElement> CustomFoodList { get; set; }

        private Patient patient;

        /// <summary>
        /// Gets or sets the patient.
        /// </summary>
        /// <value>
        /// The patient.
        /// </value>
        [DataMember]
        public Patient Patient
        {
            get { return patient; }
            set
            {
                if (value == null)
                {
                    patient = new Patient();
                }
                else
                {
                    patient = value;
                }
            }
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="Model"/> class.
        /// </summary>
        public Model()
            : this(null)
        {
            this.Registered = false;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Model"/> class.
        /// </summary>
        /// <param name="patient">The patient.</param>
        public Model(Patient patient)
            : base()
        {
            if (patient == null)
            {
                this.Patient = new Patient(this, "Prénom", "Nom", true, DateTime.Today, new Ratios(), new Contacts());
            }
            else
            {
                this.Patient = patient;
            }
            this.DefaultMealTypes = new DefaultMealTypes();
            EndLoading();
            this.MealHistoryList = new LinkedList<InsulineCalculus>();
            this.CustomFoodList = new LinkedList<FoodElement>();
            this.IsInit = false;
        }

        /// <summary>
        /// Reads the json food list.
        /// </summary>
        private async void ReadJsonFoodList()
        {
            this.FoodList = await Deserializer.deserialize();
        }

        /// <summary>
        /// Ends the loading.
        /// </summary>
        public void EndLoading()
        {
            this.AddModelManager();
            ReadJsonFoodList();
        }
    }
}
