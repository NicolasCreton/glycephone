﻿using GlycePhoneObserver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GlycePhoneModel
{
    [DataContract]
    public class ObservableModel : Observable
    {
        /// <summary>
        /// Gets or sets the model manager.
        /// </summary>
        /// <value>
        /// The model manager.
        /// </value>
        [IgnoreDataMember]
        public ModelManager ModelManager { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is initialize.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is initialize; otherwise, <c>false</c>.
        /// </value>
        [IgnoreDataMember]
        public bool IsInit { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is save.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is save; otherwise, <c>false</c>.
        /// </value>
        [IgnoreDataMember]
        public bool IsSave { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ObservableModel"/> class.
        /// </summary>
        public ObservableModel():base()
        {
            this.IsSave = false;
            this.IsInit = true;
        }

        /// <summary>
        /// Adds the model manager.
        /// </summary>
        public void AddModelManager()
        {
            this.ModelManager = ModelManager.GetInstance(this);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            IsSave = true;
            Notify();
            IsSave = false;
        }

        /// <summary>
        /// Sets the flags.
        /// </summary>
        /// <param name="b">if set to <c>true</c> [b].</param>
        public void SetFlags(bool b)
        {
            IsSave = b;
        }

        /// <summary>
        /// Notifies all.
        /// </summary>
        public void NotifyAll()
        {
            if (!IsInit)
            {
                SetFlags(true); //All flags are true so it sends to everyone
                Notify();
                SetFlags(false);
            }
        }


    }
}
