﻿using GlycePhonePatient;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace GlycePhone
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class InscriptionPageContacts : GlycePhonePage
    {
        private LinkedList<ContactForm> contactForms = new LinkedList<ContactForm>();

        private bool isFormOpen;

        /// <summary>
        /// Opens the contacts form on true, closes on false
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is form open; otherwise, <c>false</c>.
        /// </value>
        public bool IsFormOpen
        {
            get { return isFormOpen; }
            set
            {
                isFormOpen = value;
                if (isFormOpen)
                {
                    ClosedAddContactPanel.Visibility = Visibility.Collapsed;
                    OpenedAddContactPanel.Visibility = Visibility.Visible;
                    OpenAddContact.Focus(Windows.UI.Xaml.FocusState.Programmatic);
                }
                else
                {
                    OpenedAddContactPanel.Visibility = Visibility.Collapsed;
                    ClosedAddContactPanel.Visibility = Visibility.Visible;
                    CloseAddContact.Focus(Windows.UI.Xaml.FocusState.Programmatic);
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InscriptionPageContacts"/> class.
        /// </summary>
        public InscriptionPageContacts()
        {
            this.InitializeComponent();
            this.NeedSaving = true;
            this.IsRotation = true;
            if (this.Model.Patient.Contacts.People.Count == 0)
            {
                this.IsFormOpen = true;
            }
            else
            {
                this.isFormOpen = false;
            }
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            foreach (var contact in this.Model.Patient.Contacts)
            {
                this.AddNewGraphicContact(contact);
            }
        }

        /// <summary>
        /// Handles the Click event of the ValidateButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void ValidateButton_Click(object sender, RoutedEventArgs e)
        {
            if (!Model.Registered)
            {
                this.Navigate(typeof(MainMenuScreen));
                this.Model.Registered = true;
            }
            else
            {
                this.Navigate(typeof(ProfilPage));
            }
        }

        /// <summary>
        /// Handles the Click event of the GoBackButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void GoBackButton_Click(object sender, RoutedEventArgs e)
        {
            this.GoBack();
        }

        /// <summary>
        /// Handles the Click event of the ValidateContactButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void ValidateContactButton_Click(object sender, RoutedEventArgs e)
        {
            ValidateNewContact();
        }

        /// <summary>
        /// Handles the Click event of the OpenAddContact control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void OpenAddContact_Click(object sender, RoutedEventArgs e)
        {
            this.IsFormOpen = true;
        }
        /// <summary>
        /// Handles the Click event of the CloseAddContact control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void CloseAddContact_Click(object sender, RoutedEventArgs e)
        {
            this.IsFormOpen = false;
        }

        /// <summary>
        /// Validates the new contact.
        /// </summary>
        private void ValidateNewContact()
        {
            Contact newContact = ValidateNewContactForm();
            AddNewContact(newContact);
        }

        /// <summary>
        /// Adds the new graphic contact.
        /// </summary>
        /// <param name="contact">The contact.</param>
        private void AddNewGraphicContact(Contact contact)
        {
            ContactForm form = BuildContactForm(contact);
            if (form != null)
            {
                Contacts.Children.Add(form.Container);
                this.contactForms.AddLast(form);

            }
        }

        /// <summary>
        /// Adds the new contact.
        /// </summary>
        /// <param name="contact">The contact.</param>
        private void AddNewContact(Contact contact)
        {
            if (contact != null)
            {
                AddNewGraphicContact(contact);
                this.Model.Patient.Contacts.Add(contact);
            }
        }

        /// <summary>
        /// Validates the new contact form.
        /// </summary>
        /// <returns></returns>
        private Contact ValidateNewContactForm()
        {
            if (this.nom.Text == "" || this.nom.Text == "Nom" || this.numéro.Text == "" || this.numéro.Text == "Numéro")
            {
                return null;
            }
            try
            {
                Convert.ToInt64(this.numéro.Text);
                Contact newContact = new Contact(this.nom.Text, this.numéro.Text);
                this.nom.Text = "Nom";
                this.numéro.Text = "Numéro";
                return newContact;
            }
            catch //FormatException, OverflowException
            {
                return null;
            }
        }

        /// <summary>
        /// Builds the contact form.
        /// </summary>
        /// <param name="contact">The contact.</param>
        /// <returns></returns>
        private ContactForm BuildContactForm(Contact contact)
        {
            if (contact == null)
            {
                return null;
            }
            StackPanel contactPanel = new StackPanel()
            {
                Orientation = Orientation.Horizontal,
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Left,
            };

            TextBox name = new TextBox()
            {
                FontFamily = new FontFamily("/Fonts/Simpsonfont.ttf#Simpsonfont"),
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                TextAlignment = TextAlignment.Center,
                Width = 200,
                Height = 25,
                TextWrapping = TextWrapping.Wrap,
                FontSize = 15,
                Text = contact.FullName,
                MaxLength = 22,
                IsTextPredictionEnabled = false
            };
            contactPanel.Children.Add(name);

            TextBox number = new TextBox()
            {
                FontFamily = new FontFamily("/Fonts/Simpsonfont.ttf#Simpsonfont"),
                HorizontalAlignment = HorizontalAlignment.Left,
                VerticalAlignment = VerticalAlignment.Center,
                TextAlignment = TextAlignment.Center,
                Margin = new Thickness() { Left = 5 },
                Width = 150,
                Height = 25,
                TextWrapping = TextWrapping.Wrap,
                FontSize = 15,
                Text = contact.PhoneNumber,
                MaxLength = 16,
                IsTextPredictionEnabled = false,
                InputScope = new InputScope() { Names = { new InputScopeName(InputScopeNameValue.TelephoneNumber) } }
            };
            contactPanel.Children.Add(number);

            AppBarButton delete = new AppBarButton()
            {
                Icon = new SymbolIcon(Symbol.Delete),
                HorizontalAlignment = HorizontalAlignment.Left,
                VerticalAlignment = VerticalAlignment.Bottom,
                RenderTransform = new CompositeTransform() { ScaleX = 0.7, ScaleY = 0.7 },
                Height = 50,
                Width = 62,
                Margin = new Thickness() { Left = -10 }
            };
            delete.Click += delete_Click;
            contactPanel.Children.Add(delete);

            return new ContactForm(contact, name, number, delete, contactPanel);
        }

        /// <summary>
        /// Handles the Click event of the delete control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        void delete_Click(object sender, RoutedEventArgs e)
        {
            if (sender is AppBarButton)
            {
                DeleteContact((AppBarButton)sender);
            }
        }

        /// <summary>
        /// Deletes the contact.
        /// </summary>
        /// <param name="deleteButton">The delete button.</param>
        private void DeleteContact(AppBarButton deleteButton)
        {
            ContactForm form = FindContactForm(deleteButton);
            if (form != null)
            {
                Contacts.Children.Remove(form.Container);
                this.Model.Patient.Contacts.Remove(form.Contact);
            }
        }

        /// <summary>
        /// Finds the contact form from its delete button
        /// </summary>
        /// <param name="deleteButton">The delete button.</param>
        /// <returns></returns>
        private ContactForm FindContactForm(AppBarButton deleteButton)
        {
            foreach (ContactForm form in this.contactForms)
            {
                if (form.DeleteButton == deleteButton)
                {
                    return form;
                }
            }
            return null;
        }

        /// <summary>
        /// Handles the Click event of the RechercherContact control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void RechercherContact_Click(object sender, RoutedEventArgs e)
        {
            NativeContactsManager.PickContactAsync().ContinueWith(task =>
             {
                 if (task.Status == TaskStatus.RanToCompletion) { AddNewContact(NativeContactsManager.ToContact(task.Result)); }
             }, TaskScheduler.FromCurrentSynchronizationContext());

            //var contact = NativeContactsManager.PickContact();
            //BuildContactForm(contact);
        }

        /// <summary>
        /// Handles the KeyDown event of the nom control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyRoutedEventArgs"/> instance containing the event data.</param>
        private void nom_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                this.numéro.Focus(FocusState.Keyboard);
            }
        }

        /// <summary>
        /// Handles the KeyDown event of the numéro control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyRoutedEventArgs"/> instance containing the event data.</param>
        private void numéro_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                ValidateNewContact();
                this.LoseFocus(this.numéro);
            }
        }

        /// <summary>
        /// Handles the GotFocus event of the Nom control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Nom_GotFocus(object sender, RoutedEventArgs e)
        {
            if (nom.Text == "Nom")
            {
                nom.Text = "";
            }
        }
        /// <summary>
        /// Handles the LostFocus event of the Nom control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Nom_LostFocus(object sender, RoutedEventArgs e)
        {
            if (nom.Text == "")
            {
                nom.Text = "Nom";
            }
        }
        /// <summary>
        /// Handles the GotFocus event of the Numero control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Numero_GotFocus(object sender, RoutedEventArgs e)
        {
            if (numéro.Text == "Numéro")
            {
                numéro.Text = "";
            }
        }
        /// <summary>
        /// Handles the LostFocus event of the Numero control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Numero_LostFocus(object sender, RoutedEventArgs e)
        {
            if (numéro.Text == "")
            {
                numéro.Text = "Numéro";
            }
        }
    }
}
