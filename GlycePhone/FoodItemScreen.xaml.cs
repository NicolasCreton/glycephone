﻿using GlycePhoneModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour en savoir plus sur le modèle d’élément Page vierge, consultez la page http://go.microsoft.com/fwlink/?LinkID=390556

namespace GlycePhone
{
    
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class FoodItemScreen : GlycePhonePage
    {
        public FoodElement ChosenFood { get; set; }
        public float GlucideEaten { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="FoodItemScreen"/> class.
        /// This class represents a food element where we can choose the quantity eaten
        /// It displays the name of the food and its glucide
        /// </summary>
        public FoodItemScreen()
        {
            this.InitializeComponent();
            Windows.Graphics.Display.DisplayProperties.AutoRotationPreferences = Windows.Graphics.Display.DisplayOrientations.Portrait | Windows.Graphics.Display.DisplayOrientations.Landscape | Windows.Graphics.Display.DisplayOrientations.LandscapeFlipped;
        }

        
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.ChosenFood = e.Parameter as FoodElement;
            DataContext = this;
        }

        /// <summary>
        /// Handles the ValueChanged event of the GlucideSlider control.
        /// When the slider change, the calculation is made automatically to display the glucide quantity
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RangeBaseValueChangedEventArgs"/> instance containing the event data.</param>
        private void GlucideSlider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            this.EatenBox.Text = this.GlucideSlider.Value.ToString() + " grammes mangés";
            this.GlucideEaten = ((float)this.GlucideSlider.Value * this.ChosenFood.Glucide) / 100;
            this.GlucideBox.Text = this.GlucideEaten.ToString("0.00") + " grammes de glucides";
        }

        private void GoBackButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.GoBack();
        }

        private void ValidateButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.ValidateButton.Visibility = Visibility.Collapsed;
            this.ReturnButton.Visibility = Visibility.Collapsed;
            this.GlucideSlider.IsEnabled = false;
            this.ConfirmPopup.IsOpen = true;
            this.Modify.Visibility = Visibility.Visible;
        }

        private void AddButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            AddAliment();
            this.Navigate(typeof(FoodCategorieChooserScreen));
        }

        private void ModifyButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.ConfirmPopup.IsOpen = false;
            this.ValidateButton.Visibility = Visibility.Visible;
            this.ReturnButton.Visibility = Visibility.Visible;
            this.GlucideSlider.IsEnabled = true;
            this.Modify.Visibility = Visibility.Collapsed;
        }

        public void AddAliment()
        {
            this.Model.Calculus.FoodList.Add(new FoodEaten(this.ChosenFood, (float)this.GlucideSlider.Value));
            this.Model.Calculus.TotalGlucides += this.GlucideEaten;
        }

        private void FinishedButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            AddAliment();
            this.Model.Save();
            this.Navigate(typeof(EndOfTheMealScreen));
        }
    }
}
