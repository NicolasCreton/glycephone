﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlycePhoneObserver
{
    public interface IObservable
    {
        /// <summary>
        /// Adds the observer.
        /// </summary>
        /// <param name="observer">The observer.</param>
        void AddObserver(IObserver observer);
        /// <summary>
        /// Removes the observer.
        /// </summary>
        /// <param name="observer">The observer.</param>
        void RemoveObserver(IObserver observer);
        /// <summary>
        /// Notifies the observers
        /// </summary>
        void Notify();
    }
}
