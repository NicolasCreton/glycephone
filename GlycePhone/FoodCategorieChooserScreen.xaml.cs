﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Media.SpeechRecognition;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour en savoir plus sur le modèle d’élément Page vierge, consultez la page http://go.microsoft.com/fwlink/?LinkID=390556

namespace GlycePhone
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class FoodCategorieChooserScreen : GlycePhonePage
    {
        public List<FoodElement> ConcatenedList { get; set; }
        public List<IGrouping<string, FoodElement>> typeList { get; set; }
        public Boolean Search { get; set; }
        public List<object> arg { get; set; }

        private SpeechRecognizer speechRecognizer;

        /// <summary>
        /// Initializes a new instance of the <see cref="FoodCategorieChooserScreen"/> class.
        /// This class represents the screen where we can choose the type of meal
        /// The display is made using a DataTemplate with grid
        /// </summary>
        public FoodCategorieChooserScreen()
        {
            this.InitializeComponent();
            this.ConcatenedList = this.Model.CustomFoodList.Concat(this.Model.FoodList).ToList();
            this.typeList = this.ConcatenedList.GroupBy(FoodElement => FoodElement.Type).ToList();
            this.TextTileLandingGrid.ItemsSource = this.typeList;
            this.IsRotation = true;
        }



        /// <summary>
        /// Invoqué lorsque cette page est sur le point d'être affichée dans un frame.
        /// </summary>
        /// <param name="e">Données d'événement décrivant la manière dont l'utilisateur a accédé à cette page.
        /// Ce paramètre est généralement utilisé pour configurer la page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void Grid_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //List of parameter
            //1 - Search Boolean
            //2 - Food categorie
            this.Search = false;
            this.arg = new List<object>();
            arg.Add(this.Search);
            arg.Add(this.typeList[this.TextTileLandingGrid.SelectedIndex].Key);
            this.Navigate(typeof(FoodElementChooserScreen), arg);
        }

        /// <summary>
        /// Handles the GotFocus event of the SearchBox control.
        /// Delete the words in the box when we get the focus
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void SearchBox_GotFocus(object sender, RoutedEventArgs e)
        {
            this.SearchBox.Text = "";
        }

        /// <summary>
        /// Handles the LostFocus event of the SearchBox control.
        /// If the box is empty, we write the initial words in the box
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void SearchBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (this.SearchBox.Text == "")
            {
                this.SearchBox.Text = "Rechercher un aliment";
            }
        }

        private void SearchButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            ValidateSearch();
        }

        private void SearchBox_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                ValidateSearch();
            }
        }

        private void ValidateSearch()
        {
            //this.Navigate(typeof(FoodElementChooserScreen), this.SearchBox.Text);
            //List of parameter
            //1 - Search Boolean
            //2 - Food categorie
            this.Search = true;
            this.arg = new List<object>();
            arg.Add(this.Search);
            arg.Add(this.SearchBox.Text.ToLower());
            this.Navigate(typeof(FoodElementChooserScreen), arg);
        }



        /// <summary>
        /// Initializes the speech recognizer asynchronous.
        /// Contruct a SpeechRecognizer and set it
        /// </summary>
        /// <returns></returns>
        public async Task InitializeSpeechRecognizerAsync()
        {
            speechRecognizer = new SpeechRecognizer();

            //add dication grammar to the recognizer
            SpeechRecognitionTopicConstraint topicConstraint =
                new SpeechRecognitionTopicConstraint(SpeechRecognitionScenario.Dictation, "dictation");

            speechRecognizer.Constraints.Add(topicConstraint);

            //compile constraints before performing speech recognition
            await speechRecognizer.CompileConstraintsAsync();
        }

        public async Task SpeakTextAsync()
        {
            SpeechRecognitionResult recognitionResult = await speechRecognizer.RecognizeWithUIAsync();
        }


        /// <summary>
        /// Handles the Tapped event of the VocalSearch control.
        /// The vocal search will hear the words and we will take the result into a string to make the search in the list of food
        /// We need to take the string before the period beacause the vocal search add a period to the result string
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TappedRoutedEventArgs"/> instance containing the event data.</param>
        private async void VocalSearch_Tapped(object sender, TappedRoutedEventArgs e)
        {
            await InitializeSpeechRecognizerAsync();
            speechRecognizer.UIOptions.AudiblePrompt = "Quel aliment cherches-tu ?";
            speechRecognizer.UIOptions.ExampleText = "Dis le et je trouverai";
            SpeechRecognitionResult recognitionResult = await speechRecognizer.RecognizeWithUIAsync();
            int indexPoint = recognitionResult.Text.IndexOf('.');
            string stringBeforePoint = "";
            if (indexPoint >= 0)
            {
                stringBeforePoint = recognitionResult.Text.Substring(0, indexPoint);
            }
            this.Search = true;
            this.arg = new List<object>();
            arg.Add(this.Search);
            arg.Add(stringBeforePoint.ToLower());
            this.Navigate(typeof(FoodElementChooserScreen), arg);
        }


    }
}
