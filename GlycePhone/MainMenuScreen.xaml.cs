﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace GlycePhone
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainMenuScreen : GlycePhonePage
    {
        public MainMenuScreen()
        {
            this.InitializeComponent();
            this.IsRotation = false;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        /// <summary>
        /// Handles the Tapped event of the ProfilButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TappedRoutedEventArgs"/> instance containing the event data.</param>
        private void ProfilButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.Navigate(typeof(ProfilPage));
        }

        /// <summary>
        /// Handles the Tapped event of the MealButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TappedRoutedEventArgs"/> instance containing the event data.</param>
        private void MealButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.Navigate(typeof(MealTypeChooserScreen));
        }

        /// <summary>
        /// Handles the Tapped event of the UrgencyButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TappedRoutedEventArgs"/> instance containing the event data.</param>
        private void UrgencyButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.Navigate(typeof(UrgencyScreen));
        }

        /// <summary>
        /// Handles the Tapped event of the Info control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Info_Tapped(object sender, RoutedEventArgs e)
        {
            this.Navigate(typeof(AboutScreen));
        }

    }
}
