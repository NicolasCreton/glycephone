﻿using GlycePhoneModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Media.SpeechRecognition;

// Pour en savoir plus sur le modèle d’élément Page vierge, consultez la page http://go.microsoft.com/fwlink/?LinkID=390556

namespace GlycePhone
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class FoodElementChooserScreen : GlycePhonePage
    {
        public string Type { get; set; }
        public List<FoodElement> ConcatenedFoodList { get; set; }
        public List<FoodElement> QueryFoodList { get; set; }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="FoodElementChooserScreen"/> class.
        /// This class represents the screen where we can choose a food
        /// Ths display is made with grid (DataTemplate) to show each food element
        /// </summary>
        public FoodElementChooserScreen()
        {
            this.InitializeComponent();
            this.IsRotation = true;
        }


        /// <summary>
        /// Invoked when the Page is loaded and becomes the current source of a parent Frame.
        /// When we arrive in this screen we have two possibilities
        /// 1 - The screen have to display all the food of the type choosen before
        /// 2 - The screen have to show the result a search made in the screen before
        /// That's why we have two queries depending on the Boolean "Search"
        /// </summary>
        /// <param name="e">Event data that can be examined by overriding code. The event data is representative of the pending navigation that will load the current Page. Usually the most relevant property to examine is Parameter.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.ConcatenedFoodList = this.Model.CustomFoodList.Concat(this.Model.FoodList).ToList();

            List<object> arg = e.Parameter as List<object>;

            if ((Boolean)arg[0] == true)
            {
                this.QueryFoodList = (from FoodElement in (List<FoodElement>)this.ConcatenedFoodList
                                        where FoodElement.Name.ToLower().Contains((string)arg[1])
                                        select FoodElement).ToList();
                if (this.QueryFoodList.Count == 0)
                {
                    this.NoResultBlock.Visibility = Visibility.Visible; 
                }
                this.TextTileLandingGrid.ItemsSource = this.QueryFoodList;
            }
            else if ((Boolean)arg[0] == false)
            {
                this.QueryFoodList = (from FoodElement in (List<FoodElement>)this.ConcatenedFoodList
                                        where FoodElement.Type == (string)arg[1]
                                        select FoodElement).ToList();
                this.TextTileLandingGrid.ItemsSource = this.QueryFoodList;
            }
        }

        private void Grid_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.Navigate(typeof(FoodItemScreen), this.TextTileLandingGrid.SelectedItem);
        }

        private void Button_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.GoBack();
        }

        

    }
}
