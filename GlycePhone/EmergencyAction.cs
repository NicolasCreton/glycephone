﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlycePhonePatient
{
    /// <summary>
    /// Actions to be used in case of emergency
    /// </summary>
    public enum EmergencyAction
    {
        Message=1,Phone=2
    }
}
