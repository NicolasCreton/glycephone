﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Windows.UI.Xaml.Controls;

namespace GlycePhone
{
    public class MealForm
    {
        /// <summary>
        /// Gets or sets the name of the meal.
        /// </summary>
        /// <value>
        /// The name of the meal.
        /// </value>
        public string MealName { get; set; }
        /// <summary>
        /// Gets or sets the written name of the meal.
        /// </summary>
        /// <value>
        /// The name of the meal written.
        /// </value>
        public string MealWrittenName { get; set; }
        /// <summary>
        /// Gets or sets the CheckBox.
        /// </summary>
        /// <value>
        /// The CheckBox.
        /// </value>
        public CheckBox CheckBox { get; set; }
        /// <summary>
        /// Gets or sets the text box.
        /// </summary>
        /// <value>
        /// The text box.
        /// </value>
        public TextBox TextBox { get; set; }
        /// <summary>
        /// Gets or sets the container.
        /// </summary>
        /// <value>
        /// The container.
        /// </value>
        public StackPanel Container { get; set; }

        private bool editable;
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="MealForm"/> is editable. Disable or enables the edition according to this value
        /// </summary>
        /// <value>
        ///   <c>true</c> if editable; otherwise, <c>false</c>.
        /// </value>
        public bool Editable
        {
            get
            {
                return editable;
            }
            set
            {
                editable = value;
                if (!value)
                {
                    DisableEdit();
                }
                else
                {
                    EnableEdit();
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MealForm"/> class.
        /// </summary>
        public MealForm():this(String.Empty,String.Empty,true)
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MealForm"/> class.
        /// </summary>
        /// <param name="mealName">Name of the meal.</param>
        /// <param name="mealWrittenName">Name of the meal written.</param>
        public MealForm(string mealName, string mealWrittenName) : this(mealName, mealWrittenName, false) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="MealForm"/> class.
        /// </summary>
        /// <param name="mealWrittenName">Name of the meal written.</param>
        /// <param name="editable">if set to <c>true</c> [editable].</param>
        public MealForm(string mealWrittenName, bool editable) : this(String.Empty, mealWrittenName, true) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="MealForm"/> class.
        /// </summary>
        /// <param name="mealName">Name of the meal.</param>
        /// <param name="mealWrittenName">Name of the meal written.</param>
        /// <param name="editable">if set to <c>true</c> [editable].</param>
        private MealForm(string mealName, string mealWrittenName, bool editable)
        {
            this.MealName = mealName;
            this.MealWrittenName = mealWrittenName;
            this.CheckBox = new CheckBox();
            this.TextBox = new TextBox();
            this.Container = new StackPanel() { Orientation = Orientation.Horizontal };
            this.Editable = editable; //Uses the other components when set
        }

        /// <summary>
        /// Builds the CheckBox.
        /// </summary>
        /// <param name="editable">if set to <c>true</c> [editable].</param>
        private void BuildCheckBox(bool editable)
        {
            this.CheckBox.Name = this.MealName;
            if (!editable)
            {
                this.CheckBox.Content = this.MealWrittenName;
            }
            else
            {
                this.CheckBox.Content = String.Empty;
            }  
        }

        /// <summary>
        /// Builds the text box.
        /// </summary>
        /// <param name="editable">if set to <c>true</c> [editable].</param>
        private void BuildTextBox(bool editable)
        {
            if (editable)
            {
                TextBox.Text = this.MealWrittenName;
            }
        }

        /// <summary>
        /// Disables the edit.
        /// </summary>
        private void DisableEdit()
        {
            Build(false);
        }

        /// <summary>
        /// Enables the edit.
        /// </summary>
        private void EnableEdit()
        {
            Build(true);
        }

        /// <summary>
        /// Builds the specified editable.
        /// </summary>
        /// <param name="editable">if set to <c>true</c> [editable].</param>
        private void Build(bool editable)
        {
            BuildTextBox(editable);
            BuildCheckBox(editable);
            UpdateContainer();
        }

        /// <summary>
        /// Updates the container.
        /// </summary>
        public void UpdateContainer()
        {
            this.Container.Children.Clear();
            this.Container.Children.Add(CheckBox);
            if (Editable)
            {
                this.Container.Children.Add(TextBox);
            }

        }


    }
}
